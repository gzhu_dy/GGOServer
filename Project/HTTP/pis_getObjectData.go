package HTTP

import (
	"SQL/util"
	"logs"
	"net/http"
)

type friengInfo struct {
	NickName string
	Icon     string
	Custom   string
}

// request : objectID  string
// respon : friengInfo
// { nickName string
// icon     string
// custom   string}
func getObjectData(w http.ResponseWriter, r *http.Request) {

	var code = Success
	var info = "success"
	var information interface{}

	defer func() {
		writeResult(w, code, info, information)
	}()

	err := r.ParseForm()
	if err != nil {
		code = errParseForm
		logs.Print("From:getObjectData.ParseForm---", err.Error())
		info = "From:getObjectData.ParseForm---error"
		return
	}

	id := r.Form["objectID"]
	if id == nil {
		code = errEmtpyID
		info = "From:getObjectData:---objectID is null"
		return
	}

	info, code, ok := checkOutGetObjectData(id[0])
	if !ok {
		return
	}

	info, information, ok = getDbData(id[0])
	if !ok {
		code = errDBQueryFailed
		return
	}

}

//从数据库数据获取好友信息数据
func getDbData(id string) (string, interface{}, bool) {

	var nickName, icon string
	var custom []byte
	var friengInfo friengInfo
	var information []interface{}
	info := "success"

	info, _, ok := checkOutGetObjectData(id)
	if !ok {
		return info, nil, false
	}

	sql := "select nickname,icon,custom from objects where id = $1"
	rows, err := util.Query(sql, id)
	defer rows.Close()
	if err != nil {
		logs.Print("From:getObjectData.Query---", err.Error())
		info = "From:getObjectData.Query---error"
		return info, nil, false
	}

	if rows.Next() == false {
		logs.Print("From:getObjectData---- find nothing")
		info = "From:getObjectData---- find nothing"
		return info, nil, false
	}

	err = rows.Scan(&nickName, &icon, &custom)
	if err != nil {
		logs.Print("From:getObjectData.Scan---", err.Error())
		info = "From:getObjectData.Scan--- error"
		return info, nil, false
	}

	friengInfo.NickName = nickName
	friengInfo.Icon = icon
	friengInfo.Custom = string(custom)
	information = append(information, friengInfo)

	return info, friengInfo, true
}

func checkOutGetObjectData(objectID string) (string, int, bool) {
	info := "success"
	code := Success
	ok := true

	ok, code, info = checkOutId(objectID)
	if !ok {
		info = "From:getObjectData.checkOutGetObjectData:--- objectID is invlid"
		return info, code, ok
	}

	info, code, ok = isObject(objectID)
	if !ok {
		info = "From:getObjectData.checkOutGetObjectData:--- objectID is invlid because objectID is not exist"
		return info, code, ok

	}

	return info, code, ok
}
