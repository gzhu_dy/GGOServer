package HTTP

import (
	"encoding/json"
	"fmt"
	"logs"
	"net/http"
	"net/url"
)

type GroupMember struct {
	MemberID       string
	MemberIcon     string
	MemberNickname string
}

type responseJSON struct {
	Code    int
	Message string
	Result  interface{}
}

func initRequestJson(js url.Values) map[string]interface{} {
	requestJson := make(map[string]interface{})
	for key, val := range js {
		requestJson[key] = val[0]
	}
	return requestJson
}

func initHandle(w http.ResponseWriter, req *http.Request) error {

	err := req.ParseForm()
	if err != nil {
		return err
	}
	return nil
}

func writeResponse(code int, msg string, result interface{}, w http.ResponseWriter) {
	//fmt.Println("dealError")
	data, err := json.Marshal(responseJSON{
		Code:    code,
		Message: msg,
		Result:  result,
	})
	if err != nil {
		data = []byte(errorJson(err))
		logs.Print(err.Error())
	}
	if code != Success {
		logs.Print(msg)
	}
	w.Write(data)
	//logs.Print(string(data))
}

func errorJson(err error) string {
	return `{
				"code":` + fmt.Sprint(errMarshalJSONFailed) + `,
				"Message":"` + err.Error() + `",
				"Result":null
			}`
}
