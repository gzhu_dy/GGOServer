package HTTP

import (
	"encoding/json"
	"fmt"
	"logs"
	"net/http"
	"strings"
	"time"
)

type httpResult struct {
	Code    int
	Message string
	Result  interface{}
}

func (r httpResult) String() string {
	return fmt.Sprintf("httpResult\nCode:%v\nMessage:%v\nResult:%v\n", r.Code, r.Message, r.Result)
}

func writeResult(w http.ResponseWriter, code int, msg string, obj interface{}) {
	w.WriteHeader(http.StatusOK)
	res := httpResult{code, msg, obj}
	JSON, err := json.Marshal(res)
	if err != nil {
		res = httpResult{errMarshalJSONFailed, "JSON解析失败！", nil}
		logs.Print(res)
		JSON, _ = json.Marshal(res)
		if JSON == nil {
			JSON = []byte("JSON解析失败！")
		}
		w.Write(JSON)
		return
	}
	w.Write(JSON)
}
func currentTime() string {
	nowStr := time.Now().String()
	nowStrs := strings.Split(nowStr, ".")
	return nowStrs[0]
}
