package HTTP

import (
	"SQL/db"
	"logs"
	"net/http"
	"strconv"
)

type friend struct {
	FriendID   string
	Nickname   string
	FriendIcon string
}

func getFriendList(w http.ResponseWriter, req *http.Request) {
	msg, code := prepare(w, req)
	var obj interface{}
	defer func() {
		writeResult(w, code, msg, obj)
	}()
	if msg != "" {
		return
	}
	userID, _ := strconv.Atoi(req.Form["userID"][0])
	pageIndex, _ := strconv.Atoi(req.Form["pageIndex"][0])
	pageSize, _ := strconv.Atoi(req.Form["pageSize"][0])
	sql := "select id from objects where id=$1"
	rows, err := db.RowQuery(sql, userID)
	defer rows.Close()
	if rows.Next() == false {
		msg = mNotExistUserID
		code = errAccountNotExist
		return
	}
	sql = "select id,nickname,icon from objects where isUser = true and id!=$1 order by id asc limit $2 offset $3"
	offset := pageIndex * pageSize
	var args []interface{}
	args = append(args, userID, pageSize, offset)
	rows, err = db.RowQuery(sql, args...)
	if err != nil {
		logs.Print("FriendList Query error", err.Error())
		code = errDBQueryFailed
		msg = err.Error()
		return
	}
	friends := make([]friend, 0)
	var count = 0
	for rows.Next() {
		count++
		var fid string
		var icon string
		var nickname string
		err = rows.Scan(&fid, &nickname, &icon)
		if err != nil {
			logs.Print("FriendList rows.Scan error", err.Error())
			code = errRowsScan
			msg = err.Error()
			return
		}
		tem := &friend{FriendID: fid, Nickname: nickname, FriendIcon: icon}
		friends = append(friends, *tem)
	}
	if count == 0 {
		msg = "该页已经没有数据了"
		return
	}
	obj = friends
	msg = "获取好友列表成功"
}
