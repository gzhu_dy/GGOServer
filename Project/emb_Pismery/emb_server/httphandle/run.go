package httphandle

import (
	"log"
	"net/http"
)

//设置Header键值对属性
func setHeader(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
}

func Run() {
	http.HandleFunc("/getObjectData", getObjectData)
	http.HandleFunc("/sendMessage", sendMessage)
	http.HandleFunc("/alterObjectData", alterObjectData)
	http.HandleFunc("/login", login)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
