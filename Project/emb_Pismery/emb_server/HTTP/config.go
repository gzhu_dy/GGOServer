package HTTP

import (
	"emb/emb_server/SQL/util"
	"emb/emb_server/logs"
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

var globalDB = util.GetDb()

type httpResult struct {
	Code    int
	Message string
	Result  interface{}
}

func printRequestInfo(req *http.Request) {
	//logs.PrintParent(fmt.Println(time.Now(), req.RemoteAddr, req.Method, req.URL.Path))
	logs.Print(time.Now(), req.RemoteAddr, req.Method, req.URL.Path)
}

func (r httpResult) String() string {
	return fmt.Sprintf("httpResult\nCode:%v\nMessage:%v\nResult:%v\n", r.Code, r.Message, r.Result)
}

func config(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")                    //允许访问所有域
	w.Header().Add("Access-Control-Allow-Headers", "Content-Type,Origin") //header的类型
	w.Header().Add("Content-Type", "application/json")                    //返回数据格式是json

	printRequestInfo(req)
}

func writeResult(w http.ResponseWriter, code int, msg string, obj interface{}) {
	w.WriteHeader(http.StatusOK)
	res := httpResult{code, msg, obj}
	JSON, err := json.Marshal(res)
	if err != nil {
		res = httpResult{errMarshalJSONFailed, "JSON解析失败！", nil}
		logs.Print(res)
		JSON, _ = json.Marshal(res)
		if JSON == nil {
			JSON = []byte("JSON解析失败！")
		}
		w.Write(JSON)
		return
	}
	w.Write(JSON)
}
