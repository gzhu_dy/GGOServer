package HTTP

import (
	"emb/emb_server/SQL/util"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
)

// s account  string,password string
// r Message string,
//   {icon string,nickname string,isManager bool}

type userMsg struct {
	NickName  string
	Icon      string
	IsManager bool
}

func login(w http.ResponseWriter, r *http.Request) {

	var code = http.StatusOK
	var info = "success"
	var information interface{}

	fmt.Println("请求方IP地址：", r.RemoteAddr)

	//需要注意的是, defer 调用的函数参数的值 defer 被定义时就确定了.
	//因此不能 defer util.SendMsg(w, httpCode, info, information)
	defer func() {
		writeResult(w, code, info, information)
	}()

	config(w, r)

	err := r.ParseForm()
	if err != nil {
		code = http.StatusBadRequest
		info = "From:getObjectData.ParseForm---" + err.Error()
		fmt.Println("From:login.PaseForm:---", err.Error())
		return
	}

	id := r.Form["account"]
	password := r.Form["password"]

	if len(id) != 1 || id == nil {
		info = "id 输入有误"
		code = http.StatusBadRequest
		fmt.Println("An err from login:--- id != 1|| id == nil")
		return
	}
	if len(password) != 1 || password == nil {
		info = "password 输入有误"
		code = http.StatusBadRequest
		fmt.Println("An err from login:--- password != 1|| password == nil")

		return
	}
	_, err = strconv.ParseUint(id[0], 10, 64)
	if err != nil {
		info = "账号格式有误"
		code = http.StatusBadRequest
		fmt.Println("An err from login.ParseUint:---", err.Error())

		return
	}
	ok, code, info := checkOutPassword(password[0])
	if !ok {
		return
	}

	info, information, ok = checkOut(id[0], password[0])
	if !ok {
		code = http.StatusBadRequest
		return
	}
	// fmt.Println("login:informath=", information)
	fmt.Println("login:account=", id[0])
	fmt.Println("login:password=", password[0])
	fmt.Println("login: idType:", reflect.TypeOf(id[0]))
}

//检测用户密码是否正确
func checkOut(id string, pwd string) (string, interface{}, bool) {

	var info = "success"
	var userMsg userMsg
	var nickName, icon string
	var isManager bool

	sql := `select nickname,icon,users.ismanager 
			from users,objects 
			where users.id = $1 and users.password = $2 and objects.id = $3`

	db := util.GetDb()
	stmt, err := db.Prepare(sql)
	defer stmt.Close()
	if err != nil {
		info = "An error from login.checkOut.stmt:---" + err.Error()
		fmt.Println("An error from login.checkOut.stmt:---", err.Error())
		return info, nil, false
	}

	rows, err := stmt.Query(id, pwd, id)
	defer rows.Close()

	if !rows.Next() {
		info = "账号密码有误"
		return info, nil, false
	}

	err = rows.Scan(&nickName, &icon, &isManager)
	if err != nil {
		info = "An error from login.checkOut.Scan:---" + err.Error()
		fmt.Println("An error from login.checkOut.Scan:---", err.Error())
		return info, nil, false
	}

	ok, _, info := checkOutNickName(nickName)
	if !ok {
		info = "From:login.checkOut.checkOutNickName:---数据库nickname输入有误,可能过长"
		return info, nil, false
	}

	ok = checkOutIcon(&icon)
	if !ok {
		info = "From:login.checkOut.checkOutIcon:---数据库icon有误,可能路径有误"
		return info, nil, false
	}

	userMsg.NickName = nickName
	userMsg.Icon = icon
	userMsg.IsManager = isManager

	return info, userMsg, true
}
