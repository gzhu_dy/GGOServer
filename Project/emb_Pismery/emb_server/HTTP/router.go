package HTTP

import (
	"emb/emb_server/SQL/util"
	"net/http"
)

// Router 路由，按照名字分隔个人负责的接口
func Router() {

	util.InitDb()
	// util.CreateTables()

	http.HandleFunc("/getObjectData", getObjectData)
	http.HandleFunc("/sendMessage", sendMessage)
	http.HandleFunc("/alterObjectData", alterObjectData)
	http.HandleFunc("/deleteGroup", deleteGroup)
	http.ListenAndServe("127.0.0.1:8001", nil) //120.25.208.100 127.0.0.1

}
