package HTTP

import (
	"regexp"
	"unicode/utf8"
)

func checkOutId(id string) (bool, int, string) {

	info := "success"
	code := Success

	if id == "" {
		return false, errEmtpyID, mInvalidObjectID
	}

	//判断字符是否合法
	reg := regexp.MustCompile(`^[0-9]{5}$`)
	ok := reg.MatchString(id)
	if !ok {
		return false, errInvalidID, mInvalidObjectID
	}

	return true, code, info

}

func checkOutJsonInfo(jsonInfo string) (bool, int, string) {

	if jsonInfo == "" || jsonInfo == " " {
		return true, Success, "success"
	}
	//判断字符是否合法
	reg := regexp.MustCompile("^{.*\\r*\\n*}")
	ok := reg.MatchString(jsonInfo)
	if !ok {
		return false, errInvalidCustomInfo, mInvalidCustomInfo
	}
	return true, Success, "success"
}

func checkOutNickName(nickName string) (bool, int, string) {

	if utf8.RuneCountInString(nickName) > 200 {
		return false, errInvalidNickname, mInvalidNickname
	}
	return true, Success, "success"
}

func checkOutIcon(icon *string) bool {

	//如果为空 设置icon默认值
	if *icon == "" {
		*icon = "default Icon"
	}

	return true
}

func checkOutPassword(password string) (bool, int, string) {

	//判断密码长度是否过长
	if utf8.RuneCountInString(password) > 200 {
		return false, errPasswordWrong, mInvalidPassword
	}

	//判断字符是否合法
	reg := regexp.MustCompile("^[\\da-zA-Z-_\\.\\?\\!\\,\\@\\#\\$\\%\\^\\&\\*\\(\\)\\=\\+\\[\\]\\{\\}]{6,20}$")
	ok := reg.MatchString(password)
	if !ok {
		return false, errPasswordWrong, mInvalidPassword
	}

	return true, Success, "success"
}
