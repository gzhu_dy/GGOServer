package util

import (
	"bytes"
	"fmt"
	"os"
	"regexp"
	"runtime/debug"
	"strings"
	"unicode"
	"unicode/utf8"
)

func Test01() {
	str := "aaabb123"
	l1 := len([]rune(str))
	l2 := bytes.Count([]byte(str), nil) - 1
	l3 := strings.Count(str, "") - 1
	l4 := utf8.RuneCountInString(str)
	fmt.Println(l1)
	fmt.Println(l2)
	fmt.Println(l3)
	fmt.Println(l4)
}

func Test02() {
	str := "aaaaaa"
	for _, r := range str {
		if unicode.Is(unicode.Scripts["Han"], r) {
			fmt.Println("是中文")
		}
	}
	fmt.Println("不是中文")

}

func Test03() {
	str := ""
	fmt.Println(str)
	aaa(&str)
	fmt.Println(str)

}

func aaa(str *string) {
	if *str == "" {
		*str = "aaa"
	}
}

func double(x int) int {
	return x + x
}

func triple(x int) (r int) {
	defer func() {
		r += x
	}()
	return double(x)
}

func Test04() {
	fmt.Println(triple(3))
}

func Test05() {
	i := 1
	f := 1.10
	s := "st"
	fmt.Fprintln(os.Stderr, "aaaa", i)
	fmt.Fprintln(os.Stderr, f)
	fmt.Fprintln(os.Stderr, s)
	fmt.Println("aaaa", i)
	str := fmt.Sprintf("%x", i)
	fmt.Fprint(os.Stderr, str)

}

func checkOutId(id string) bool {

	if id == "" {
		return false
	}

	reg := regexp.MustCompile("^[\\da-zA-Z-_\\.\\?\\!\\,\\@\\#\\$\\%\\^\\&\\*\\(\\)\\=\\+\\[\\]\\{\\}]{6,20}$")
	ok := reg.MatchString(id)
	if !ok {
		return false
	}

	return true

}

func Test06() {
	id := "1112345asd-_!@#$"
	fmt.Println(checkOutId(id))
	Test08()
	Test07()
}

func Test07(args ...interface{}) {

	fmt.Println("-------------------------------")

	str := string(debug.Stack())

	strline := strings.Split(str, "\n")
	a := []interface{}{strline[6] + ":"}
	for i, j := range a {
		fmt.Println(i, ":---", j)
	}
	a = append(a, args...)
	for i, j := range a {
		fmt.Println(i, ":---", j)
	}
	// fmt.Println(a...)
}

func Test08() {
	Test07("a", "s", "d", "d", "f")
}

func checkOutJsonInfo(jsonInfo string) bool {
	//判断字符是否合法
	reg := regexp.MustCompile("^{.*\\r*\\n*}")
	ok := reg.MatchString(jsonInfo)
	if !ok {
		return false
	}
	return true
}

func Test09() {
	println(checkOutJsonInfo("{asda//nsd}"))
}

func Test10() {
	// fmt.Printf("%q\n", strings.Split("a,b,c", ","))
	// fmt.Printf("%q\n", strings.Split("a man a plan a canal panama", "a "))
	// fmt.Printf("%q\n", strings.Split(" xyz ", ""))
	fmt.Println(strings.Split("", ";"))
	// fmt.Println(strings.TrimSpace(" \t\n a lone gopher \n\t\r\n"))
	strArray := strings.Split(strings.Trim("{}", "{} "), ";")
	fmt.Println(strArray[0])
	fmt.Println(len(strArray))
	for i := 0; i < len(strArray); i++ {
		s1 := strings.Split(strArray[i], ":")
		fmt.Println(len(s1))
		for k, v := range s1 {
			fmt.Println(k, ":", v)
		}
	}
}

func convertJson(jsonInfo string) (map[string]interface{}, bool) {

	result := make(map[string]interface{})

	//jsonInfo = "{sex:male;age:17;friend:[asd,asd,asd]}"
	//strArray = [sex:male	age:17	friend:[asd,asd,asd]]
	strArray := strings.Split(strings.Trim(jsonInfo, "{sex:male*age:18*friend:[1,2,3,4]} "), "*")

	for i := 0; i < len(strArray); i++ {
		valueAarry := strings.Split(strArray[i], ":")
		if len(valueAarry) > 2 {
			return nil, false
		}
		result[valueAarry[0]] = valueAarry[1]
	}

	return result, true
}

func Test11() {
	strArray, _ := convertJson("{}")
	fmt.Println(strArray)
}
