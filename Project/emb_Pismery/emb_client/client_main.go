package main

import (
	"emb/emb_client/interfaces"
	"fmt"
)

func main() {
	fmt.Println("*******************************************************")
	test()
}

func test() {
	interfaces.GetObjectData()
	interfaces.SendMessage()
	interfaces.AlterObjectData()
	interfaces.GetObjectData()
	interfaces.DeleteGroup()
	interfaces.GetObjectData()

}
