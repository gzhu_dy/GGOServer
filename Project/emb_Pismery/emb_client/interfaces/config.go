package interfaces

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	hostURL  = "http://127.0.0.1:8001"
	userID   = "10001"
	password = "123456"
)

func post(url string, data url.Values) {
	resp, err := http.PostForm(hostURL+url, data)
	if err != nil {
		fmt.Println("错误：", err.Error())
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("错误：", err.Error())
		return
	}
	fmt.Println(string(body))

}
