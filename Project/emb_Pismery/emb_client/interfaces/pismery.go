package interfaces

import (
	"net/url"
)

func AlterObjectData() {
	data := url.Values{
		"objectID":   {"10001"},
		"nickname":   {"alter_pismery"},
		"icon":       {"alter_icon"},
		"customInfo": {"{\"sex\":\"male\"}"},
	}
	post("/alterObjectData", data)
}

func GetObjectData() {
	data := url.Values{
		"objectID": {"10004"},
	}
	post("/getObjectData", data)
}

func SendMessage() {
	data := url.Values{
		"userID":     {"10002"},
		"talkerID":   {"10001"},
		"msgContent": {"test msg"},
	}
	post("/sendMessage", data)
}

func DeleteGroup() {
	data := url.Values{
		"groupID": {"10004"},
		"userID":  {"10003"},
	}
	post("/deleteGroup", data)
}
