package logs

import (
	"fmt"
	"runtime/debug"
	"strings"
)

const debugMode = true

// Print 打印调用此函数的外层函数的文件和行号信息
//
// -- parent's parent func
// 		|
//		-- parent func  //打印这个函数的信息
// 			|
//			-- PrintParent
//
func Print(args ...interface{}) {
	if !debugMode {
		return
	}

	stack := string(debug.Stack())
	stackLines := strings.Split(stack, "\n")
	debugLine := strings.SplitAfter(stackLines[6], "/")
	debugInfo := debugLine[len(debugLine)-1]
	debugInfo = debugInfo[:len(debugInfo)-6]
	a := []interface{}{debugInfo + ":"}
	a = append(a, args...)
	fmt.Println(a...)
}

// PrintParent 打印调用此函数的外层函数的外层函数的文件和行号信息
//
// -- parent's parent func  //打印这个函数的信息
// 		|
//		-- parent func
// 			|
//			-- PrintParent
//
// 注意：main调用此函数，程序会崩溃！
func PrintParent(args ...interface{}) {
	if !debugMode {
		return
	}
	stack := string(debug.Stack())
	stackLines := strings.Split(stack, "\n")
	debugLine := strings.SplitAfter(stackLines[8], "/")
	debugInfo := debugLine[len(debugLine)-1]
	debugInfo = debugInfo[:len(debugInfo)-6]
	a := []interface{}{debugInfo + ":"}
	a = append(a, args...)
	fmt.Println(a...)
}
