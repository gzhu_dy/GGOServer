#! /bin/bash

export GOPATH=/home/ficow/gopath:`cd .. ; pwd`
echo "GOPATH:$GOPATH"

export GOROOT=/home/ficow/go
echo "GOROOT:$GOROOT"

PATH=$PATH:$HOME/bin:usr/local/git/bin:$GOROOT/bin:$GOPATH/bin:
echo "PATH:$PATH"

(go run emb.go >> log.txt 2>&1 &)

echo "*** 服务器重启完成！ ***" >> log.txt

echo "--------------------------- `date`" >> log.txt