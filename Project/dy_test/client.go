package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func randString() string {
	var s []byte
	for l := rand.Uint32()%10 + 1; l > 0; l-- {
		s = append(s, uint8(rand.Uint32()%26+'a'))
	}
	//println("rand",string(s))
	return string(s)
}

var ch chan int

var socket string = "http://127.0.0.1:8080"

func post(form *url.Values, URI string) {
	fmt.Println(form)
	resp, _ := http.PostForm(socket+URI, *form)
	fmt.Println("resp:\n", resp)
	if resp == nil {
		println("login fail")
		return
	}
	println(resp.StatusCode)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))
	resp.Body.Close()
	//post1(username)
}
func testAddGroup() {
	form := url.Values{"nickname": {"addGroup"}, "groups": {"newGroup"}}
	post(&form, "/addGroup")
}
func testAlterGroupData() {
	form := url.Values{"groupID": {"10003"}, "nickname": {"alterGroupdata"}, "标签": {"后端"}}
	post(&form, "/alterGroupdata")
}
func testAddGroupMember() {
	form := url.Values{"groupID": {"10003"}, "pageIndex": {"0"}, "pageSize": {"3"}}
	post(&form, "/addGroupMember")
}
func testConfirmAddGroupMember() {
	form := url.Values{"groupID": {"10003"}, "memberID": {"10001", "10002"}}
	post(&form, "/confirmAddGroupMember")
}
func testDeleteGroupMember() {
	form := url.Values{"groupID": {"10003"}, "memberID": {"10001", "10002"}}
	post(&form, "/deleteGroupMember")
}
func testSendMessage() {
	//form := url.Values{"userID": {"10001"}, "friendOrGroupID": {"10003"}, "msgContent": {"hello world!"}}
	form := url.Values{"userID": {"10004"}, "friendOrGroupID": {"10003"}, "msgContent": {"hello world!"}}
	//form := url.Values{}
	post(&form, "/sendMessage")
}
func testGetFriendData() {
	form := url.Values{"friendID": {"10001"}}
	post(&form, "/getFriendData")
}
func testGetGroupMember() {
	form := url.Values{"groupID": {"10003"}, "pageIndex": {"0"}, "pageSize": {"3"}}
	post(&form, "/getGroupMember")
}
func main() {
	//testConfirmAddGroupMember()
	testGetGroupMember()
	testAddGroupMember()
	//testDeleteGroupMember()
	//testAlterGroupData()
	//testAddGroup()
	//testSendMessage()
	//testGetFriendData()
}

func post1(s string) {
	//ch<-1
	for true {
		time.Sleep(time.Second * 10)
		resp, _ := http.PostForm("http://127.0.0.1:8080/check", url.Values{"username": {s}})
		if resp == nil {
			println("post1 no response")
			return
		}
		defer resp.Body.Close()
		println("check online :", s)
	}
}
func DoRequest() {
	for i := 0; i < 100; i++ {
		transport := http.Transport{
			DisableKeepAlives: true,
		}
		client := &http.Client{
			Transport: &transport,
		}

		username := randString()
		password := randString()
		req, err := http.NewRequest("POST", "http://127.0.0.1:8080/login",
			strings.NewReader(url.Values{"username": {username}, "password": {password}}.Encode()))
		if err != nil {
			println("error:", err.Error())
		}

		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Set("Cookie", "name=anny")
		req.Header.Set("username", username)
		req.Header.Set("password", password)
		println("login	username:", username, "password:", password)
		resp, err := client.Do(req)
		if err != nil {
			println("error:", err.Error())
		}
		if resp == nil {
			println("login no response")
			return
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			println("error:", err.Error())
		}

		fmt.Println(string(body))
		go post1(username)
	}
}
