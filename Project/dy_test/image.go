package main

import (
	"fmt"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"os"
)

func main() {
	fImg1, err := os.Open("1.png")
	if err != nil {
		panic(err)
	}
	defer fImg1.Close()
	img1, s, err := image.Decode(fImg1)
	if err != nil {
		fmt.Println(s, err.Error())
		return
	}
	fmt.Println("s:", s)
	//fImg2, _ := os.Open("2.jpg")
	//defer fImg2.Close()
	//img2, _, _ := image.Decode(fImg2)
	//m := image.NewRGBA(img1.Bounds())
	//draw.Draw(m, m.Bounds(), img1, image.Point{0, 0}, draw.Src)
	//draw.Draw(m, m.Bounds(), img2, image.Point{-200,-200}, draw.Src)
	toimg, err := os.Create("new.jpg")
	if err != nil {
		panic(err)
	}
	defer toimg.Close()
	jpeg.Encode(toimg, img1, &jpeg.Options{100})
}
