package main

import (
	"time"
)

func main() {
	var lock chan bool
	go func() {
		lock <- true
		time.Sleep(1)
	}()
}
