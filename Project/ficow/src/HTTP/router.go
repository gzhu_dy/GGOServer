﻿package HTTP

import (
	"SQL/FSDB"
	"logs"
	"net/http"
)

const addr = "120.25.208.100:8023"

// const addr = ":8001"

type router struct {
}

func (r router) ServeHTTP(w http.ResponseWriter, req *http.Request) {

	config(w, req)
	switch req.URL.Path {
	case "/ws":
		initWebSocket(w, req)
	case "/dbPage":
		dbPage(w, req)
	case "/reset":
		reset(w, req)
	case "/login":
		login(w, req)
	case "/chatMessageList":
		chatMessageList(w, req)
	case "/sessionListWithFriend":
		sessionListWithFriend(w, req)
	case "/sessionListWithGroup":
		sessionListWithGroup(w, req)
	case "/sendMessage":
		sendMessage(w, req)
	case "/getObjectData":
		getObjectData(w, req)
	case "/alterObjectData":
		alterObjectData(w, req)
	case "/getFriendList":
		getFriendList(w, req)
	case "/getGroupList":
		getGroupList(w, req)
	case "/getGroupMember":
		getGroupMember(w, req)
	case "/addGroup":
		addGroup(w, req)
	case "/addGroupMember":
		addGroupMember(w, req)
	case "/confirmAddGroupMember":
		confirmAddGroupMember(w, req)
	case "/deleteGroup":
		deleteGroup(w, req)
	case "/deleteGroupMember":
		deleteGroupMember(w, req)
	default:
		http.NotFound(w, req)
	}
}

// LaunchRouter 路由，按照名字分隔个人负责的接口
func LaunchRouter() {

	err := FSDB.Open()
	if err != nil {
		logs.Print(err.Error())
		panic(err)
	}
	resetDB()
	// http.HandleFunc("/ws", initWebSocket)

	//http.HandleFunc("/dbPage", dbPage)
	// http.HandleFunc("/login", login)
	// http.HandleFunc("/chatMessageList", chatMessageList)
	// http.HandleFunc("/sessionListWithFriend", sessionListWithFriend)
	// http.HandleFunc("/sessionListWithGroup", sessionListWithGroup)

	// http.HandleFunc("/sendMessage", sendMessage)
	// http.HandleFunc("/getObjectData", getObjectData)
	// http.HandleFunc("/alterObjectData", alterObjectData)
	// http.HandleFunc("/getFriendList", getFriendList)
	// http.HandleFunc("/getGroupList", getGroupList)
	// http.HandleFunc("/getGroupMember", getGroupMember)

	// http.HandleFunc("/addGroup", addGroup)
	// http.HandleFunc("/addGroupMember", addGroupMember)
	// http.HandleFunc("/confirmAddGroupMember", confirmAddGroupMember)
	// http.HandleFunc("/deleteGroupMember", deleteGroupMember)

	//--------------------------------------------------Ficow Shen
	go dispatchMessages()
	r := &router{}
	http.ListenAndServe(addr, r)
}
