﻿package HTTP

// HTTP errorCode for JSON
// 如需使用自定义的errorCode，按照规则添加即可
// 命名规则： err + 自定义名称 = 与现存错误码不冲突的错误码
// 			2xx 代表成功
// 			4xx 代表前端请求有错误
// 			5xx 代表后端处理异常

const (
	Success = 200
	// 250之后的错误码用于/ws接口，具体请看fs_websocket.go内的定义

	errAccountError            = 401
	errAccountNotExist         = 402
	errPasswordWrong           = 403
	errArgsAllEmpty            = 404
	errEmtpyID                 = 405
	errNotInt                  = 406
	errNegativeInt             = 407
	errRequestEntityTooLarge   = 408 //请求的东西太大(pageSize 太大)
	errEmptyParameter          = 409
	errNoPermission            = 410
	errInvalidArgLength        = 412
	errWrongContentType        = 413
	errArgIsNotNumber          = 414
	errGroupIDNotExist         = 415
	errAccountAndPasswordError = 416

	errInvalidID           = 417
	errInvalidPassword     = 419
	errInvalidTime         = 420
	errInvalidNickname     = 421
	errInvalidIcon         = 422
	errInvalidPageIndex    = 423
	errInvalidPageSize     = 424
	errInvalidCustomInfo   = 425
	errInvalidMsgContent   = 426
	errInvalidBoolArg      = 427
	errInvalidEmptyArgs    = 428
	errInvalidTalker       = 429
	errNotExistUserInGroup = 430
	errInvalidManager      = 431
	errInvalidSender       = 432
	errInvalidReceiver     = 433
	errInvalidMsg          = 434

	errNotExistID = 444

	errMarshalJSONFailed = 501
	errDBQueryFailed     = 502
	errDBExecFailed      = 503
	errRowsScan          = 504
	errOpenDB            = 505
	errBeginTx           = 506
	errParseForm         = 507
	errEndTx             = 508
)

const (
	mInvalidUserID       = "UserID参数有误，正确的UserID是长度为5数字序列！"
	mNotExistUserID      = "UserID不存在！"
	mInvalidMemberID     = "MemberID参数有误，正确的MemberID是长度为5数字序列！"
	mNotExistMemberID    = "MemberID不存在！"
	mInvalidGroupID      = "GroupID参数有误，正确的GroupID是长度为5数字序列！"
	mNotExistGroupID     = "GroupID不存在！"
	mInvalidObjectID     = "ObjectID参数有误，正确的ObjectID是长度为5数字序列！"
	mNotExistObjectID    = "ObjectID不存在！"
	mInvalidTalkerID     = "TalkerID参数有误，正确的TalkerID是长度为5数字序列！"
	mNotExistTalkerID    = "TalkerID不存在！"
	mNotExistUserInGroup = "当前用户不在此群中！"
	mInvalidAccount      = "Account参数有误，正确的Account是长度为5数字序列！"
	mInvalidPassword     = "Password参数有误，正确的Password是长度为6~20的字符序列，可包含数字、大小写字母及符号,.-_=+!@#$%^&*()[]{} "
	mInvalidTime         = "Time参数有误，正确的Time应采用此格式：yyyy-mm-dd hh:mm:ss，日期与时间之间有一个空格。"
	mInvalidNickname     = "Nickname参数有误，正确的Nickname是长度为1~18的字符序列！"
	mInvalidIcon         = "Icon参数有误，正确的Icon是长度为1~18的字符序列！"
	mInvalidPageIndex    = "PageIndex参数有误，正确的PageIndex是长度为1~6的正整数！"
	mInvalidPageSize     = "PageSize参数有误，正确的PageSize是长度为1~3的正整数！"
	mInvalidCustomInfo   = "CustomInfo参数有误，正确的CustomInfo是长度为0~1024的JSON序列!"
	mInvalidMsgContent   = "MsgContent参数有误，正确的MsgContent是长度为0~1024的JSON序列！"
	mInvalidBoolArg      = "Bool类型的参数有误，不是正确的布尔型参数！"
	mInvalidEmptyArgs    = "参数不能为空，请检查相关参数！并检查Content-Type是否为application/x-www-form-urlencoded。"
	mInvalidChat         = "UserID和TalkerID相同，消息发送失败，用户不能发送消息给自己！"
	mInvalidManager      = "此用户不是管理员，不可以删除群组！"
	mInvalidSender       = "亲，消息发送人应该是您本人哦！听话，不要冒充别人，好吗？"
	mInvalidReceiver     = "亲，把消息发给自己是不是很有意思呢？求求您，别给服务器增加负担啦！"
	mInvalidMsg          = "消息的长度应为1~2048！"
)
