package HTTP

import (
	"SQL/FSDB"
	"net/http"
)

func reset(w http.ResponseWriter, req *http.Request) {

	w.Header().Add("Content-Type", "text/html; charset=utf-8")
	printRequestInfo(req)
	resetDB()

	w.Header().Add("Content-Type", "text/html")

	w.Write([]byte(`
		<!DOCTYPE html>
		<html>
		<head>
		<title>重置成功</title>
		</head>
		<body>
			<script>window.location.href="/dbPage"</script> 
			<a href=/>若没有跳转请点这里</a>
		<body>
		</html>
	`))
}

func resetDB() {
	_, _ = FSDB.Exec(`DROP TABLE IF EXISTS messages`)
	_, _ = FSDB.Exec(`DROP TABLE IF EXISTS members`)
	_, _ = FSDB.Exec(`DROP TABLE IF EXISTS groups`)
	_, _ = FSDB.Exec(`DROP TABLE IF EXISTS users`)
	_, _ = FSDB.Exec(`DROP TABLE IF EXISTS objects`)

	_, _ = FSDB.Exec(`
		create table if not exists objects (
			id serial8 primary key,
			nickname varchar(200) not null,			
			icon varchar(500),	
			isUser bool not null,
			custom jsonb
		);
	`)
	_, _ = FSDB.Exec(`select setval('objects_id_seq',10000);--设置起始值`)
	_, _ = FSDB.Exec(`insert into objects(nickname,icon,isUser,custom) values('用户1','icon1',true,'{"sex":"female","age":18,"同学":"[10003,10002]"}'::json);`)
	_, _ = FSDB.Exec(`insert into objects(nickname,icon,isUser,custom) values('测试用户2','icon2',true,'{"sex":"male","age":19,"崇拜的人":"[10004]","同学":"[10003,10001]"}'::json);`)
	_, _ = FSDB.Exec(`insert into objects(nickname,icon,isUser,custom) values('TestUser3','icon3',true,'{"sex":"female","age":20,"男朋友":"[10004]","同学":"[10001,10002]"}'::json);`)
	_, _ = FSDB.Exec(`insert into objects(nickname,icon,isUser,custom) values('User4','icon4',true,'{"sex":"male","age":22,"女朋友":"[10003]"}'::json);`)
	_, _ = FSDB.Exec(`insert into objects(nickname,icon,isUser,custom) values('嵌入式讨论群5','icon5',false,'{"地址":"广州"}'::json);`)
	_, _ = FSDB.Exec(`insert into objects(nickname,icon,isUser,custom) values('吹水群6','icon6',false,'{"地址":"佛山","群组类型":"闲聊"}'::json);`)

	_, _ = FSDB.Exec(`
		create table if not exists users (
			id int8 references objects(id) on delete cascade,
			password varchar(200) not null,
			isManager bool not null,
			lastOnlineTime timestamp with time zone default now(),
			primary key(id)
		);
		
	`)
	_, _ = FSDB.Exec(`insert into users(id,password,isManager) values(10001,'123456',false);`)
	_, _ = FSDB.Exec(`insert into users(id,password,isManager) values(10002,'123456',true);`)
	_, _ = FSDB.Exec(`insert into users(id,password,isManager) values(10003,'123456',true);`)
	_, _ = FSDB.Exec(`insert into users(id,password,isManager) values(10004,'123456',false);`)

	_, _ = FSDB.Exec(`
		create table if not exists groups (
			id int8 references objects(id) on delete cascade,
			createTime timestamp with time zone default now(),
			primary key(id)
		);
	`)
	_, _ = FSDB.Exec(`insert into groups(id) values(10005);`)
	_, _ = FSDB.Exec(`insert into groups(id) values(10006);`)

	_, _ = FSDB.Exec(`
		create table if not exists members (
			groupID int8 references objects(id) on delete cascade,
			userID int8 references objects(id) on delete cascade,
			primary key(groupID,userID)
		);
	`)
	_, _ = FSDB.Exec(`insert into members(groupID,userID) values (10005,10001),(10005,10002),(10005,10004);`)
	_, _ = FSDB.Exec(`insert into members(groupID,userID) values (10006,10001),(10006,10002),(10006,10003);`)

	_, _ = FSDB.Exec(`
		create table if not exists messages (
			id serial8 primary key,
			content varchar(2000) not null,
			sender int8 references objects(id) on delete cascade,
			receiver int8 references objects(id) on delete cascade,
			createTime timestamp with time zone default now()
		);
	`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10001:你好 user2!我要给你发一条很长的消息，然后让你可以测试一下你的消息气泡的高度是不是有问题！',10001,10002);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10002:嘿 user1!在吗？',10002,10001);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10002:嘿 user4!呃，没事。。。',10002,10004);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10003:你好 user1! user1。。。。。。。',10003,10001);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10001:嘿 group5!我要给你发一条很长的消息，然后让你可以测试一下你的消息气泡的高度是不是有问题！正常吗？',10001,10005);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10001:嘿 group6!呃，我来测试一下你的消息气泡的高度是不是有问题！',10001,10006);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10002:嗨 group5!我要给你发一条很长很长很长的消息，然后让你可以测试一下你的消息气泡的高度是不是有问题！正常吗？要不然，我再说点啥？你再看下，现在看下正常不？？？？？？？',10002,10005);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10003:嗨 group6! 1.0版本啥时候诞生呢？',10003,10006);`)
	_, _ = FSDB.Exec(`insert into messages(content,sender,receiver) values('10004:你好 group6!我也要给你发一条，一条很长很长的消息，然后让你可以测试一下你的消息气泡的高度是不是有问题！正常了吧？',10004,10006);`)
}
