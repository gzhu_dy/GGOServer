package HTTP

import (
	"SQL/FSDB"
	"net/http"
)

type groupInfo struct {
	GroupID  string
	Nickname string
	Icon     string
}

func getGroupList(w http.ResponseWriter, req *http.Request) {

	var resCode = http.StatusOK
	var msg = ""
	var res interface{}
	defer func() {
		writeResult(w, resCode, msg, res)
	}()

	userID := req.FormValue("userID")
	pageIndex := req.FormValue("pageIndex")
	pageSize := req.FormValue("pageSize")

	ok := false
	if ok, resCode, msg = isArgsEmpty(userID, pageIndex, pageSize); ok {
		return
	}
	if ok, resCode, msg = isValidID(userID, "UserID"); !ok {
		return
	}
	pSize := 0
	if ok, pSize, resCode, msg = isValidPageSize(pageSize); !ok {
		return
	}
	pIndex := 0
	if ok, pIndex, resCode, msg = isValidPageIndex(pageIndex); !ok {
		return
	}

	errCode, errMsg, groupList := getGroupListFromDB(userID, pIndex, pSize)
	msg = errMsg
	if errCode != Success {
		resCode = errCode
		return
	}
	res = groupList
}

func getGroupListFromDB(userID string, pageIndex int, pageSize int) (errCode int, errMsg string, groupList interface{}) {

	groupListSQL := `SELECT id,nickname,icon 
						FROM objects o,members m 
						WHERE m.userid=$1 
							AND m.groupid=o.id
						LIMIT $2 
						OFFSET $3;`

	rows, err := FSDB.Query(groupListSQL, userID, pageSize, pageIndex)
	defer rows.Close()
	if err != nil {
		return errDBExecFailed, err.Error(), nil
	}
	groupRows := make([]groupInfo, 0)
	for rows.Next() {
		var (
			id       string
			icon     string
			nickname string
		)
		err := rows.Scan(&id, &icon, &nickname)
		if err != nil {
			return errDBExecFailed, err.Error(), nil
		}
		info := groupInfo{id, icon, nickname}
		groupRows = append(groupRows, info)
	}
	return Success, "获取群组列表成功！", groupRows
}
