package HTTP

import (
	"SQL/FSDB"
	"net/http"
)

func deleteGroup(w http.ResponseWriter, req *http.Request) {

	var resCode = http.StatusOK
	var msg = ""
	var res interface{}
	defer func() {
		writeResult(w, resCode, msg, res)
	}()

	userID := req.FormValue("userID")
	groupID := req.FormValue("groupID")

	ok := false
	if ok, resCode, msg = isExistMangagerID(userID); !ok {
		return
	}
	if ok, resCode, msg = isValidID(groupID, "GroupID"); !ok {
		return
	}

	errCode, errMsg := deleteGroupFromDB(groupID)
	msg = errMsg
	if errCode != Success {
		resCode = errCode
		return
	}
}

func deleteGroupFromDB(groupID string) (errCode int, errMsg string) {

	delGroupSQL := `DELETE FROM objects WHERE id = $1;`
	_, err := FSDB.Exec(delGroupSQL, groupID)

	if err != nil {
		return errDBExecFailed, err.Error()
	}
	return Success, "删除群成功！"
}
