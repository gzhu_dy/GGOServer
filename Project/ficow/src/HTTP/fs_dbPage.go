package HTTP

import (
	"SQL/FSDB"
	"fmt"
	"net/http"
)

type HTML struct {
	Content string //exported field since it begins with a capital letter
}

func dbPage(w http.ResponseWriter, req *http.Request) {

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	printRequestInfo(req)

	resultStr := ""

	defer func() {

		resultStr += fmt.Sprintln(`
		</body>
		</html>
		`)
		w.Write([]byte(resultStr))
	}()

	resultStr += fmt.Sprintln(`
		<!DOCTYPE html>
		<html>
		<head>
		<title>数据库:8023</title>
		</head>
		<body>
	`)

	resultStr += fmt.Sprintln(`
		<a href="\reset"><h1>重置数据库<h1></a>
	`)

	resultStr += fmt.Sprintln(`
		<h1>以下是数据库中的所有数据：（8023端口 - Ficow 测试专用）</h1>
	`)
	resultStr += dataFromObjects()
	resultStr += dataFromUsers()
	resultStr += dataFromGroups()
	resultStr += dataFromMembers()
	resultStr += dataFromMessages()

}

func dataFromObjects() (resultStr string) {

	rows, err := FSDB.Query("select id,nickname,icon,isUser,custom from objects")
	defer rows.Close()
	if err != nil {
		resultStr += fmt.Sprintln("<p>" + err.Error() + "</p>")
		return
	}
	resultStr += fmt.Sprintln(`
		<h2>1.objects(id,nickname,icon,isUser,custom)，包含所有用户和组</h2>
		<table border="1">
			<tr>
				<th>id</th>
				<th>nickname</th>
				<th>icon</th>
				<th>isUser</th>
				<th>custom</th>
			</tr>
	`)
	for rows.Next() {
		var (
			id       string
			nickname string
			icon     string
			isUser   string
			custom   string
		)
		rows.Scan(&id, &nickname, &icon, &isUser, &custom)
		resultStr += fmt.Sprintln(`
			<tr>
				<td>` + id + `</td>
				<td>` + nickname + `</td>
				<td>` + icon + `</td>
				<td>` + isUser + `</td>
				<td>` + custom + `</td>
			</tr>
		`)
	}
	resultStr += fmt.Sprintln(`
		</table>
	`)
	return
}

func dataFromUsers() (resultStr string) {

	rows, err := FSDB.Query("select id,password,isManager,lastOnlineTime from users")
	defer rows.Close()
	if err != nil {
		resultStr += fmt.Sprintln("<p>" + err.Error() + "</p>")
		return
	}
	resultStr += fmt.Sprintln(`
		<h2>2.users(id,password,isManager)</h2>
		<table border="1">
			<tr>
				<th>id</th>
				<th>password</th>
				<th>isManager</th>
				<th>lastOnlineTime</th>
			</tr>
	`)
	for rows.Next() {
		var (
			id             string
			password       string
			isManager      string
			lastOnlineTime string
		)
		rows.Scan(&id, &password, &isManager, &lastOnlineTime)
		resultStr += fmt.Sprintln(`
			<tr>
				<td>` + id + `</td>
				<td>` + password + `</td>
				<td>` + isManager + `</td>
				<td>` + lastOnlineTime + `</td>
			</tr>
		`)
	}
	resultStr += fmt.Sprintln(`
		</table>
	`)
	return
}

func dataFromGroups() (resultStr string) {

	rows, err := FSDB.Query("select id,createTime from groups")
	defer rows.Close()
	if err != nil {
		resultStr += fmt.Sprintln("<p>" + err.Error() + "</p>")
		return
	}
	resultStr += fmt.Sprintln(`
		<h2>3.groups(id,createTime)</h2>
		<table border="1">
			<tr>
				<th>id</th>
				<th>createTime</th>
			</tr>
	`)
	for rows.Next() {
		var (
			id         string
			createTime string
		)
		rows.Scan(&id, &createTime)
		resultStr += fmt.Sprintln(`
			<tr>
				<td>` + id + `</td>
				<td>` + createTime + `</td>
			</tr>
		`)
	}
	resultStr += fmt.Sprintln(`
		</table>
	`)
	return
}

func dataFromMembers() (resultStr string) {

	rows, err := FSDB.Query("select groupID,userID from members")
	defer rows.Close()
	if err != nil {
		resultStr += fmt.Sprintln("<p>" + err.Error() + "</p>")
		return
	}
	resultStr += fmt.Sprintln(`
		<h2>4.members(groupID,userID),此表表示哪些人在哪些群</h2>
		<table border="1">
			<tr>
				<th>groupID</th>
				<th>userID</th>
			</tr>
	`)
	for rows.Next() {
		var (
			groupID string
			userID  string
		)
		rows.Scan(&groupID, &userID)
		resultStr += fmt.Sprintln(`
			<tr>
				<td>` + groupID + `</td>
				<td>` + userID + `</td>
			</tr>
		`)
	}
	resultStr += fmt.Sprintln(`
		</table>
	`)
	return
}

func dataFromMessages() (resultStr string) {

	rows, err := FSDB.Query("select id,content,sender,receiver,createTime from messages")
	defer rows.Close()
	if err != nil {
		resultStr += fmt.Sprintln("<p>" + err.Error() + "</p>")
		return
	}
	resultStr += fmt.Sprintln(`
		<h2>5.messages(id,content,sender,receiver,createTime)</h2>
		<table border="1">
			<tr>
				<th>id</th>
				<th>content</th>
				<th>sender</th>
				<th>receiver</th>
				<th>createTime</th>
			</tr>
	`)
	for rows.Next() {
		var (
			id         string
			content    string
			sender     string
			receiver   string
			createTime string
		)
		rows.Scan(&id, &content, &sender, &receiver, &createTime)
		resultStr += fmt.Sprintln(`
			<tr>
				<td>` + id + `</td>
				<td>` + content + `</td>
				<td>` + sender + `</td>
				<td>` + receiver + `</td>
				<td>` + createTime + `</td>
			</tr>
		`)
	}
	resultStr += fmt.Sprintln(`
		</table>
	`)
	return
}
