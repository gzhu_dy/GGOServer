package HTTP

import (
	"SQL/FSDB"
	"net/http"
)

type memberInfo struct {
	MemberID   string
	MemberIcon string
}

func getGroupMember(w http.ResponseWriter, req *http.Request) {

	var resCode = http.StatusOK
	var msg = ""
	var res interface{}
	defer func() {
		writeResult(w, resCode, msg, res)
	}()

	groupID := req.FormValue("groupID")
	pageIndex := req.FormValue("pageIndex")
	pageSize := req.FormValue("pageSize")

	ok := false
	if ok, resCode, msg = isArgsEmpty(groupID, pageIndex, pageSize); ok {
		return
	}
	if ok, resCode, msg = isValidID(groupID, "GroupID"); !ok {
		if resCode == errInvalidID {
			msg = mInvalidGroupID
		}
		return
	}
	pSize := 0
	if ok, pSize, resCode, msg = isValidPageSize(pageSize); !ok {
		return
	}
	pIndex := 0
	if ok, pIndex, resCode, msg = isValidPageIndex(pageIndex); !ok {
		return
	}

	errCode, errMsg, memberList := getGroupMemberListFromDB(groupID, pIndex, pSize)
	msg = errMsg
	if errCode != Success {
		resCode = errCode
		return
	}
	res = memberList
}

func getGroupMemberListFromDB(groupID string, pageIndex int, pageSize int) (errCode int, errMsg string, memberList interface{}) {

	friendListSQL := `SELECT id,icon 
						FROM objects o,members m 
						WHERE m.groupid=$1 AND o.id=m.userid
						LIMIT $2 
						OFFSET $3;`

	rows, err := FSDB.Query(friendListSQL, groupID, pageSize, pageIndex)
	defer rows.Close()
	if err != nil {
		return errDBExecFailed, err.Error(), nil
	}
	memberRows := make([]memberInfo, 0)
	for rows.Next() {
		var (
			id   string
			icon string
		)
		err := rows.Scan(&id, &icon)
		if err != nil {
			return errDBExecFailed, err.Error(), nil
		}
		info := memberInfo{id, icon}
		memberRows = append(memberRows, info)
	}
	return Success, "获取群组成员列表成功！", memberRows
}
