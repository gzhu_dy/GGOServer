package HTTP

import (
	"SQL/FSDB"
	"logs"
	"net/http"
)

type chatMessageRow struct {
	MsgID      string
	SenderID   string
	Nickname   string
	IconURL    string
	MsgContent string
	SendTime   string
}

func chatMessageList(w http.ResponseWriter, req *http.Request) {

	var resCode = Success
	var msg = ""
	var res interface{}
	defer func() {
		writeResult(w, resCode, msg, res)
	}()

	userID := req.FormValue("userID")
	talkerID := req.FormValue("talkerID")
	talkerIsUser := req.FormValue("talkerIsUser")
	pageIndex := req.FormValue("pageIndex")
	pageSize := req.FormValue("pageSize")

	ok := false
	if ok, resCode, msg = isArgsEmpty(userID, talkerID, talkerIsUser, pageIndex, pageSize); ok {
		return
	}
	if ok, resCode, msg = isValidID(userID, "UserID"); !ok {
		return
	}
	if ok, resCode, msg = isValidID(talkerID, "TalkerID"); !ok {
		return
	}
	if userID == talkerID {
		msg = mInvalidChat
		resCode = errInvalidTalker
		return
	}
	isTalkerUser := false
	if ok, isTalkerUser, resCode, msg = isValidBool(talkerIsUser); !ok {
		return
	}
	if isTalkerUser {
		if ok, resCode, msg = isExistTalkerID(userID, talkerID, true); !ok {
			return
		}
	} else {
		if ok, resCode, msg = isExistTalkerID(userID, talkerID, false); !ok {
			return
		}
	}

	pSize := 0
	if ok, pSize, resCode, msg = isValidPageSize(pageSize); !ok {
		return
	}
	pIndex := 0
	if ok, pIndex, resCode, msg = isValidPageIndex(pageIndex); !ok {
		return
	}

	errCode, errMsg, rows := getMsgListFromDB(userID, talkerID, isTalkerUser, pIndex, pSize)
	msg = errMsg
	if errCode != Success {
		resCode = errCode
		return
	}
	res = rows
}

func getMsgListFromDB(userID string, talkerID string, isUser bool, pageIndex int, pageSize int) (errCode int, errMsg string, msgList interface{}) {

	var msgRows []chatMessageRow

	/*	与用户聊天的消息：
		SELECT m.id,m.content,m.createTime
		FROM objects o,messages m
		WHERE m.receiver = 10001 AND m.sender = 10002
		ORDER BY m.createTime DESC
		LIMIT 20 OFFSET 10;
	*/
	var sql = ""
	var args []interface{}
	if isUser {
		sql = `SELECT m.id,m.sender,o.nickname,o.icon,m.content,m.createTime
				FROM messages m,objects o
				WHERE (
                    m.receiver = $1
		 			AND m.sender = $2
					AND o.id = m.sender
                	)OR(
                    m.receiver = $3
		 			AND m.sender = $4
					AND o.id = m.sender
                    )
				ORDER BY m.createTime DESC
		  		LIMIT $5 OFFSET $6;`
		args = append(args, userID, talkerID, talkerID, userID)
	} else {
		sql = `SELECT m.id,m.sender,o.nickname,o.icon,m.content,m.createTime 
				FROM messages m,objects o
				WHERE m.receiver = $1
					AND o.id = m.sender
				ORDER BY m.createTime DESC
				LIMIT $2 OFFSET $3;`
		args = append(args, talkerID)
	}
	args = append(args, pageSize, pageSize*pageIndex)

	rows, err := FSDB.Query(sql, args...)
	defer rows.Close()
	if err != nil {
		logs.Print("getMsgListFromDB 出错，err:", err.Error())
		return errDBQueryFailed, err.Error(), nil
	}

	for rows.Next() {
		var mid string
		var senderID string
		var nickname string
		var icon string
		var content string
		var createTime string
		err := rows.Scan(&mid, &senderID, &nickname, &icon, &content, &createTime)
		if err != nil {
			logs.Print("getMsgListFromDB rows.Scan出错，err:", err.Error())
			return errDBQueryFailed, err.Error(), nil
		}
		row := chatMessageRow{mid, senderID, nickname, icon, content, createTime}
		msgRows = append(msgRows, row)
	}
	return Success, "成功获取 " + userID + " 与 " + talkerID + " 的聊天消息！", msgRows
}
