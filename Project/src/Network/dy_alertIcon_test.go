package Network

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
)

func testAlterIcon() {
	test := []testData{
		{
			data: map[string]string{
				"token":    websocketToken,
				"objectID": "100000001",
			},
			msg:     "icon",
			expCode: Success,
		},
		{
			data: map[string]string{
				"token":    websocketToken,
				"objectID": "100000000",
			},
			msg:     "icon",
			expCode: errNoPermission,
		},
		{
			data: map[string]string{
				"token":    websocketToken,
				"objectID": "100000005",
			},
			msg:     "icon",
			expCode: errNoPermission,
		},
		{
			data: map[string]string{
				"token": websocketToken,
			},
			msg:     "icon",
			expCode: Success,
		},
		{
			data: map[string]string{
				"objectID": "100000005",
			},
			msg:     "icon",
			expCode: errInvalidToken,
		},
		{
			data: map[string]string{
				"token": websocketToken,
			},
			msg:     "file",
			expCode: errParseFileFailed,
		},
	}
	for idx, _ := range test {
		data, ok := test[idx].data.(map[string]string)
		if !ok {
			panic(test[idx].data)
		}
		req, err := newfileUploadRequest("http://"+socket+"/alterIcon", data, test[idx].msg, "../public/icon/default.ico")
		if err != nil {
			panic(err)
		}
		test[idx].data = req
	}
	test = append(test, testData{
		data: url.Values{
			"token":    {websocketToken},
			"objectID": {"100000001"},
			"icon":     {""},
		},
		msg:     "http://" + socket + "/alterIcon",
		expCode: errParseForm,
	})
	httpTest(test)
}

func newfileUploadRequest(uri string, form map[string]string, formFileName, path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile(formFileName, filepath.Base(path))
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, file)
	if err != nil {
		return nil, err
	}

	for key, val := range form {
		err = writer.WriteField(key, val)
		if err != nil {
			return nil, err
		}
	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", uri, body)
	req.Header.Add("Content-Type", writer.FormDataContentType())
	if err != nil {
		return nil, err
	}
	return req, nil
}
