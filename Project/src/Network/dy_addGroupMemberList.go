package Network

import (
	"SQL/myDB"
)

type GroupMember struct {
	ID       string
	IconURL  string
	Nickname string
}

//获取<添加群成员>界面的成员列表：
//s groupID string,pageIndex int,pageSize int
//r Message string,
//  [memeberID string,memberIcon string,memberNickname string]//添加memberIcon

func addGroupMemberList(userID string, data dy_judge) (code int, msg string, result interface{}) {
	code, msg, groupID := data.judgeID("groupID")
	if code != Success {
		return
	}
	code, msg = IsGroupManager(userID, groupID)
	if code != Success {
		return
	}
	code, msg, pageIndex := data.judgePageIndex("pageIndex")
	if code != Success {
		return
	}
	code, msg, pageSize := data.judgePageSize("pageSize")
	if code != Success {
		return
	}
	offset := pageIndex * pageSize
	return dealAddGroupMemberList(groupID, offset, pageSize)
}
func dealAddGroupMemberList(groupID string, ofset int, pageSize int) (code int, msg string, result interface{}) {
	rows, err := myDB.QuerySql(`select id,iconURL,nickname
							 	from objects 
							 	where isUser = true and id not in (
								  	select userID from members 
								  	where groupID=$1 ) 
							 	limit $2 offset $3`, groupID, pageSize, ofset)
	if err != nil {
		code = errDBQueryFailed
		msg = err.Error()
		return
	}
	defer rows.Close()

	groupMember := make([]GroupMember, 0)
	for rows.Next() {
		var (
			ID       string
			iconURL  string
			nickname string
		)
		err = rows.Scan(&ID, &iconURL, &nickname)
		if err != nil {
			code = errRowsScan
			msg = err.Error()
			break
		}
		groupMember = append(groupMember, GroupMember{
			ID:       ID,
			IconURL:  iconURL,
			Nickname: nickname,
		})
	}
	return Success, "", groupMember
}
