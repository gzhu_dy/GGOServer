package Network

import (
	"SQL/util"
	"logs"
	"net/http"
)

// s account,oldPassword,newPassword string
// r Message string,
func alterPassword(w http.ResponseWriter, r *http.Request) (code int, info string, information interface{}) {

	code = Success
	info = "success"

	accountList := r.Form["account"]
	oldPwdList := r.Form["oldPassword"]
	newPwdList := r.Form["newPassword"]

	if accountList == nil {
		info = "From:alterPassword:---account is null"
		code = errInvalidAccount
		return
	}
	if oldPwdList == nil {
		info = "From:alterPassword:---oldPwd is null"
		code = errInvalidAccount
		return
	}
	if newPwdList == nil {
		info = "From:alterPassword:---newPassword is null"
		code = errInvalidAccount
		return
	}

	info, code, ok := checkOutAlterPassowrd(accountList[0], oldPwdList[0], newPwdList[0])
	if !ok {
		return
	}

	info, code, ok = dealAlterPassword(accountList[0], oldPwdList[0], newPwdList[0])
	return
}

func checkOutAlterPassowrd(account, oldPwd, newPwd string) (info string, code int, ok bool) {
	info = "success"
	code = Success
	ok = true

	if ok, code, info = checkOutAccount(account); !ok {
		info = "From:checkOutAlterPassowrd.checkOutAccount:--- account is invalid"
		return
	}

	if oldPwd == newPwd {
		info = "From:checkOutAlterPassowrd:--- oldPassword equal newPassowrd"
		code = errInvalidPassword
		ok = false
		return
	}

	if ok, code, info = checkOutPassword(oldPwd); !ok {
		info = "From:checkOutAlterPassowrd.checkOutPassword:--- oldPwd is invalid"
		return
	}
	if ok, code, info = checkOutPassword(newPwd); !ok {
		info = "From:checkOutAlterPassowrd.checkOutPassword:--- newPwd is invalid"
		return
	}

	if info, code, ok = isAccountExist(account); !ok {
		info = "From:checkOutAlterPassowrd.isAccountExist:--- account is not exist"
		return
	}
	return
}

func dealAlterPassword(account, oldPwd, newPwd string) (info string, code int, ok bool) {

	info = "success"
	code = Success
	ok = true

	sql := "select password from users where account = $1"
	rows, stmt, err := util.Query(sql, account)
	defer stmt.Close()
	defer rows.Close()
	if err != nil {
		logs.Print("From:dealAlterPassword.Query:---", err.Error())
		info = "From:dealAlterPassword.Query:--- error"
		code = errDBQueryFailed
		ok = false
		return info, code, ok
	}

	var pwd string
	if rows.Next() {
		err = rows.Scan(&pwd)
		if err != nil {
			logs.Print("From:dealAlterPassword.rows.Scan:---", err.Error())
			info = "From:dealAlterPassword.rows.Scan:--- error"
			code = errRowsScan
			ok = false
			return info, code, ok
		}
	}
	if pwd != oldPwd {
		info = "From:dealAlterPassword:--- oldPassword is error"
		code = errInvalidPassword
		ok = false
		return info, code, ok
	}

	sql = "update users set password = $1 where account = $2"
	result, err := util.Exec(sql, newPwd, account)
	if err != nil {
		logs.Print("From:dealAlterPassword.Exec:---", err.Error())
		info = "From:dealAlterPassword.Exec:--- error"
		code = errDBExecFailed
		ok = false
		return info, code, ok
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		logs.Print("From:dealAlterPassword.RowsAffected:---", err.Error())
		info = "From:dealAlterPassword.RowsAffected:---error"
		code = errRowsAffected
		ok = false
		return info, code, ok
	}
	if rowsAffected <= 0 {
		info = "From:dealAlterPassword.RowsAffected:---update rows is 0"
		code = errDBExecFailed
		ok = false
		return info, code, ok
	}
	return
}
