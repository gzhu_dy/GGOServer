package Network

import (
	"SQL/FSDB"

	"logs"

	_ "github.com/lib/pq"
)

type SessionInfo struct {
	FriendSessions interface{} `json:"FriendSessions"`
	GroupSessions  interface{} `json:"GroupSessions"`
}

func sessionList(clientID string, args interface{}) (response *ServerResponse) {

	var errCode = Success
	var errMsg = ""
	var result interface{}
	defer func() {
		response = &ServerResponse{
			Method:  "/sessionList",
			Code:    errCode,
			Message: errMsg,
			Result:  result,
		}
	}()

	tx, db, err := FSDB.BeginTx()
	if err != nil {
		errCode = errEndTx
		errMsg = "FSDB.BeginTx 执行错误：" + err.Error()
		return
	}
	defer func() {
		defer closeDB(db)
		if errCode == Success {
			err := tx.Commit()
			if err != nil {
				errCode = errEndTx
				errMsg = err.Error()
				logs.Print("/sessionList tx.Commit err:", err.Error())
			}
			return
		}
		err := tx.Rollback()
		if err != nil {
			errCode = errEndTx
			errMsg = err.Error()
			logs.Print("*sessionList tx.Rollback err:", err.Error())
		}
	}()

	var friendSessions interface{}
	errCode, errMsg, friendSessions = friendSessionInContext(tx, clientID)
	if errCode != Success {
		return
	}
	var groupSessions interface{}
	errCode, errMsg, groupSessions = groupSessionInContext(tx, clientID)
	if errCode != Success {
		return
	}

	sessionInfo := SessionInfo{
		FriendSessions: friendSessions,
		GroupSessions:  groupSessions,
	}
	result = sessionInfo

	// // 更新用户在线时间
	// updateUserOnlineTime(tx, clientID, currentTime())
	return
}
