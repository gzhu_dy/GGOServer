// zd_initws
package Network

type zd_argmap map[string]interface{}

func initargs(args interface{}, argsname []string) (msg string, code int, argsmap zd_argmap) {
	Args, ok := args.(map[string]interface{})
	if ok {
		argsmap = Args
		count := 0
		for k, v := range Args {
			for i := 0; i < len(argsname); i++ {
				if k == argsname[i] {
					count++
					val, ok := v.(string)
					if !ok {
						return errAssertMsg, errAssertType, nil
					}
					msg, code = checkArgs(k, val)
					if code != Success {
						return msg, code, nil
					}
					break
				}
			}
		}
		if count == len(argsname) {
			return "", Success, argsmap
		} else {
			return "缺少参数，请检查", errlackParam, nil
		}
	}
	return mInvalidEmptyArgs, errInvalidEmptyArgs, nil
}
