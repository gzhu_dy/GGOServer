package Network

import (
	"encoding/json"
	"io/ioutil"
	"logs"
	"net/http"
	"net/url"
	"strings"
	"testing"
	"time"
)

type testData struct {
	id      string
	data    interface{}
	code    int
	msg     string
	result  interface{}
	expCode int
}

var websocketToken string
var socket string = "127.0.0.1:8001"

func TestWsRouter(test *testing.T) {
	publicFileDir = "../public"
	privateFileDir = "../private"
	go LaunchRouter()
	for len(dbQueue) <= 0 {
		time.Sleep(time.Millisecond * 1000)
	}
	dyTest([]testData{{data: `
{
	"Method":"/createGroup",
	"Args":""
}`, expCode: errAssertType}})
	testAddGroupMemberList()
	testAddGroupMember()
	testCreateGroup()
	testDeleteGroupMember()
	/*---------------------http-----------------------*/
	testWsLogin()
	testAlterIcon()

}

func dyTest(test []testData) {
	for idx, _ := range test {
		val := &test[idx]
		var req ClientRequest
		var res *ServerResponse
		err := json.Unmarshal([]byte(val.data.(string)), &req)
		if err != nil {
			logs.Print(err.Error())
			panic(err)
		}
		res = dyRouter(req.Method, val.id, req.Args)
		val.code = res.Code
		val.msg = res.Message
		val.result = res.Result
		if val.code != val.expCode {
			logs.Print(val)
			panic(val)
		}
	}
}

func httpTest(test []testData) {
	var err error
	for idx, _ := range test {
		val := &test[idx]
		var req *http.Request
		switch tmp := val.data.(type) {
		case *http.Request:
			req = tmp
		case url.Values:
			req, err = http.NewRequest("POST", val.msg, strings.NewReader(tmp.Encode()))
			if err != nil {
				panic(err)
			}
		default:
			logs.Print(val)
			panic(val)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			panic(err)
		}
		dealHttpResp(resp, val)
	}
}
func dealHttpResp(resp *http.Response, test *testData) {
	defer resp.Body.Close()
	contentType := resp.Header["Content-Type"][0]
	if strings.Index(contentType, "json") != -1 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
		}
		var res HTTPResult
		err = json.Unmarshal(body, &res)
		if err != nil {
			panic(err)
		}
		test.code = res.Code
		test.msg = res.Message
		test.result = res.Result
		if test.code != test.expCode {
			logs.Print(test)
			panic(test)
		}
	}
}
