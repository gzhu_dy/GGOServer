package Network

import (
	"SQL/myDB"
)

//删除群成员：
//s groupID string,[memberIDs string]
//r Message string,
//  nil
func deleteGroupMember(userID string, data dy_judge) (code int, msg string, result interface{}) {
	code, msg, groupID := data.judgeID("groupID")
	if code != Success {
		return
	}
	code, msg = IsOwnerOfGroup(userID, groupID)
	if code != Success {
		return
	}
	code, msg, memberIDs := data.judgeMemberIDs("memberIDs")
	if code != Success {
		return
	}
	if IsUserIDInMemberID(userID, memberIDs) {
		return errInvalidID, "不能删除自己", nil
	}

	return dealDeleteGroupMember(groupID, memberIDs)
}
func dealDeleteGroupMember(groupID string, memberIDs []string) (code int, msg string, result interface{}) {
	db := openDB()
	defer closeDB(db)

	tx, err := db.Begin()
	if err != nil {
		code = errBeginTx
		msg = err.Error()
		return
	}

	defer func() {
		if code != Success {
			err := tx.Rollback()
			if err != nil {
				code = errEndTx
				msg = err.Error()
			}
		}
	}()

	err = myDB.TxExecMultiSql(tx, "delete from members where groupID=$2 and userID=$1", memberIDs, groupID)
	if err != nil {
		code = errDBExecFailed
		msg = err.Error()
		return
	}

	err = tx.Commit()
	if err != nil {
		code = errEndTx
		msg = err.Error()
	}
	return Success, "", nil
}
