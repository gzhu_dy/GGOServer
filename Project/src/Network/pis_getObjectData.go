package Network

import (
	"SQL/util"
	"logs"
)

type userInfo struct {
	ObjectID       string `json:"objectID"`
	Account        string `json:"account"`
	Nickname       string `json:"nickname"`
	Icon           string `json:"icon"`
	Custom         string `json:"custom"`
	LastOnlineTime string `json:"lastOnlineTime"`
}

type groupInfo struct {
	ObjectID   string `json:"objectID"`
	Nickname   string `json:"nickname"`
	Icon       string `json:"icon"`
	Custom     string `json:"custom"`
	Owner      string `json:"owner"`
	Createtime string `json:"createTime"`
}

// request : objectID  string
// respon : friengInfo
// { nickName string
// icon     string
// custom   string}
func getObjectData(clientID string, args interface{}) (response *ServerResponse) {
	var code = Success
	var info = "success"
	var information interface{}

	defer func() {
		response = &ServerResponse{
			Method:  "/getObjectData",
			Code:    code,
			Message: info,
			Result:  information,
		}
	}()

	parameterMap, ok := args.(map[string]interface{})
	if !ok {
		code = errInvalidEmptyArgs
		info = mInvalidEmptyArgs
		return
	}

	objectID, ok := parameterMap["objectID"].(string)
	if !ok {
		info = "From:getObjectData:---objectID is null"
		code = errInvalidID
		return
	}

	if objectID == "000000000" {
		objectID = clientID
	}

	info, code, ok = checkOutGetObjectData(objectID)
	if !ok {
		return
	}

	info, information, ok = getDbData(objectID)
	if !ok {
		code = errAccountError
		return
	}
	return
}

func checkOutGetObjectData(objectID string) (string, int, bool) {
	info := "success"
	code := Success
	ok := true

	ok, code, info = checkOutId(objectID)
	if !ok {
		info = "From:getObjectData.checkOutGetObjectData:--- objectID is invlid"
		return info, code, ok
	}

	info, code, ok = isObject(objectID)
	if !ok {
		info = "From:getObjectData.checkOutGetObjectData:--- objectID is invlid because objectID is not exist"
		return info, code, ok

	}

	return info, code, ok
}

func getDbData(id string) (string, interface{}, bool) {

	var information []interface{}
	info := "success"

	info, _, ok := checkOutGetObjectData(id)
	if !ok {
		return info, nil, false
	}

	_, _, ok = isUser(id)
	if ok {
		//如果是user
		var userInfo userInfo
		userInfo.ObjectID = id

		sql := `select o.nickname,o.iconurl,o.custom,u.account,u.lastonlinetime 
				from objects o,users u 
				where o.id = u.id and o.id= $1`
		rows, stmt, err := util.Query(sql, id)
		if stmt != nil {
			defer stmt.Close()
		}
		if rows != nil {
			defer rows.Close()
		}

		if err != nil {
			logs.Print("From:getObjectData.Query---", err.Error())
			info = "From:getObjectData.Query---error"
			return info, nil, false
		}

		if rows.Next() == false {
			logs.Print("From:getObjectData---- find nothing")
			info = "From:getObjectData---- find nothing"
			return info, nil, false
		}

		err = rows.Scan(&userInfo.Nickname, &userInfo.Icon, &userInfo.Custom, &userInfo.Account, &userInfo.LastOnlineTime)
		if err != nil {
			logs.Print("From:getObjectData.Scan---", err.Error())
			info = "From:getObjectData.Scan--- error"
			return info, nil, false
		}

		information = append(information, userInfo)
		return info, information, true
	}

	//如果是群
	var groupInfo groupInfo
	groupInfo.ObjectID = id

	sql := `select o.nickname,o.iconurl,o.custom,g.owner,g.createtime
				from objects o,groups g 
				where o.id = g.id and o.id= $1`
	rows, stmt, err := util.Query(sql, id)
	if stmt != nil {
		defer stmt.Close()
	}
	if rows != nil {
		defer rows.Close()
	}

	if err != nil {
		logs.Print("From:getObjectData.Query---", err.Error())
		info = "From:getObjectData.Query---error"
		return info, nil, false
	}

	if rows.Next() == false {
		logs.Print("From:getObjectData---- find nothing")
		info = "From:getObjectData---- find nothing"
		return info, nil, false
	}

	err = rows.Scan(&groupInfo.Nickname, &groupInfo.Icon, &groupInfo.Custom, &groupInfo.Owner, &groupInfo.Createtime)
	if err != nil {
		logs.Print("From:getObjectData.Scan---", err.Error())
		info = "From:getObjectData.Scan--- error"
		return info, nil, false
	}

	information = append(information, groupInfo)
	return info, information, true
}
