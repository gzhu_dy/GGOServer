// zd_getFriendList
package Network

import (
	"SQL/db"
	"logs"
	"strconv"
)

type friend struct {
	FriendID   string
	Nickname   string
	FriendIcon string
}

func getFriendList(clientID string, args interface{}) (response *ServerResponse) {
	code := Success
	msg := ""
	var res interface{}
	defer func() {
		if code != Success {
			logs.Print("code: ", code, " msg: ", msg)
		}
		response = &ServerResponse{
			Method:  "/getFriendList",
			Code:    code,
			Message: msg,
			Result:  res,
		}
	}()
	argsname := []string{"pageIndex", "pageSize"}
	msg, code, argsmap := initargs(args, argsname)
	if code != Success {
		return
	}
	userID := clientID
	pageIndex, _ := strconv.Atoi(argsmap["pageIndex"].(string))
	pageSize, _ := strconv.Atoi(argsmap["pageSize"].(string))
	sql := "select id from objects where id=$1"
	rows, err := db.RowQuery(sql, userID)
	if err != nil {
		logs.Print(err.Error())
		msg = err.Error()
		code = errDBQueryFailed
		return
	}
	defer rows.Close()
	if rows.Next() == false {
		msg = mNotExistUserID
		code = errAccountNotExist
		return
	}
	sql = `select id,nickname,iconurl 
		from objects
		where isUser = true and id!=$1 
		order by id asc limit $2 offset $3`
	offset := pageIndex * pageSize
	var objs []interface{}
	objs = append(objs, userID, pageSize, offset)
	rows, err = db.RowQuery(sql, objs...)
	if err != nil {
		logs.Print("FriendList Query error", err.Error())
		code = errDBQueryFailed
		msg = err.Error()
		return
	}
	friends := make([]friend, 0)
	var count = 0
	for rows.Next() {
		count++
		var fid string
		var icon string
		var nickname string
		err = rows.Scan(&fid, &nickname, &icon)
		if err != nil {
			logs.Print("FriendList rows.Scan error", err.Error())
			code = errRowsScan
			msg = err.Error()
			return
		}
		tem := &friend{FriendID: fid, Nickname: nickname, FriendIcon: icon}
		friends = append(friends, *tem)
	}
	if count == 0 {
		msg = "该页已经没有数据了"
		return
	}
	res = friends
	msg = "获取好友列表成功"
	return
}
