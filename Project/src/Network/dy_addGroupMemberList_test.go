package Network

func testAddGroupMemberList() {
	var test []testData = []testData{
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMemberList",
	"Args":{
		"groupID":"100000006",
		"pageIndex":0,
		"pageSize":10
		}
}
`, expCode: Success},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMemberList",
	"Args":{
		"groupID":100000006,
		"pageIndex":0,
		"pageSize":10
		}
}
`, expCode: errAssertType},
		{
			id: "100000001",
			data: `
{
	"Method":"/addGroupMemberList",
	"Args":{
		"groupID":"100000006",
		"pageIndex":0,
		"pageSize":10
		}
}
`, expCode: errNoPermission},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMemberList",
	"Args":{
		"groupID":"100000006",
		"pageIndex":-1,
		"pageSize":10
		}
}
`, expCode: errInvalidPageIndex},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMemberList",
	"Args":{
		"groupID":"100000006",
		"pageIndex":"0",
		"pageSize":10
		}
}
`, expCode: errAssertType},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMemberList",
	"Args":{
		"groupID":"100000006",
		"pageIndex":0,
		"pageSize":100
		}
}
`, expCode: errInvalidPageSize},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMemberList",
	"Args":{
		"groupID":"100000006",
		"pageIndex":0,
		"pageSize":"100"
		}
}
`, expCode: errAssertType},
	}
	dyTest(test)
}
