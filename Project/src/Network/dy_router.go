package Network

import (
	"logs"
)

func dyRouter(URL string, userID string, req interface{}) (response *ServerResponse) {
	var (
		code   int         = Success
		msg    string      = ""
		result interface{} = nil
	)
	defer func() {
		if code != Success {
			logs.Print("code:", code, "\tmsg:", msg, "\tresult", result)
		}
		response = &ServerResponse{
			Method:  URL,
			Code:    code,
			Message: msg,
			Result:  result,
		}
	}()
	dataMap, ok := req.(map[string]interface{})
	if !ok {
		code = errAssertType
		msg = "type isn't map[string]interface{}"
		return
	}
	var data dy_judge = dataMap
	switch URL {
	case "/createGroup":
		code, msg, result = createGroup(userID, data)
		break
	case "/addGroupMember":
		code, msg, result = addGroupMember(userID, data)
		break
	case "/addGroupMemberList":
		code, msg, result = addGroupMemberList(userID, data)
		break
	case "/deleteGroupMember":
		code, msg, result = deleteGroupMember(userID, data)
		break
	default:
		code = errBadAPI
		msg = "dy_router default"
		return
	}
	return
}
