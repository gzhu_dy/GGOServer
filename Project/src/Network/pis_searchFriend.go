package Network

import (
	"SQL/util"
	"logs"
)

type friendInfo struct {
	FriendIcon string `json:"FriendIcon"`
	Nickname   string `json:"Nickname"`
	FriendID   string `json:"FriendID"`
}

func searchFriend(clientID string, args interface{}) (response *ServerResponse) {

	var code = Success
	var info = "success"
	var information interface{}

	defer func() {
		response = &ServerResponse{
			Method:  "/searchFriend",
			Code:    code,
			Message: info,
			Result:  information,
		}
	}()

	parameterMap, ok := args.(map[string]interface{})
	if !ok {
		code = errInvalidEmptyArgs
		info = mInvalidEmptyArgs
		return
	}

	searchParam, ok := parameterMap["searchParam"].(string)
	if !ok {
		code = errSearchParam
		info = "From:searchFrind:---searchParam is null"
		return
	}

	//如果输入""则返回空的内容
	if len(searchParam) == 0 {
		var friendsInfo []friendInfo
		information = friendsInfo
		return
	}

	info, code, information, _ = dealSearchFriend(searchParam)
	return 
}

func dealSearchFriend(searchParam string) (info string, code int, result interface{}, ok bool) {
	info = "success"
	code = Success
	ok = true
	var friendsInfo []friendInfo

	/*sql := `select o.id,o.iconurl,o.nickname
	from objects o,users u
	where o.id = u.id and o.isuser = true
		and (o.custom ->'真实姓名' ? $1
		or o.custom ->'公司' ? $1
		or o.custom ->'职位' ? $1
		or o.custom ->'部门' ? $1
		or o.custom ->'地址' ? $1
		or o.custom ->'电子邮箱' ? $1
		or o.custom ->'网站' ? $1
		or o.custom ->'生日' ? $1
		or o.custom ->'备注' ? $1
		or o.nickname = $1
		or u.account = $1)`*/
	sql := `SELECT o.id,o.iconurl,o.nickname
			FROM objects o,users u,(select id
				,CAST(custom ->'真实姓名' AS character varying(500)) realname
				,CAST(custom ->'公司' AS character varying(500)) company	
				,CAST(custom ->'职位' AS character varying(500)) post
				,CAST(custom ->'部门' AS character varying(500)) department
				,CAST(custom ->'地址' AS character varying(500)) address
				,CAST(custom ->'电子邮箱' AS character varying(500)) mailbox
				,CAST(custom ->'网站' AS character varying(500)) websit
				,CAST(custom ->'生日' AS character varying(500)) birthday
				,CAST(custom ->'备注' AS character varying(500)) remarks
				from objects) c
			WHERE o.id = c.id
				AND o.id = u.id
				AND o.isuser = true
				AND (c.realname like $1 
					OR c.company like $1 
					OR c.post like $1 
					OR c.department like $1 
					OR c.address like $1 
					OR c.mailbox like $1 
					OR c.websit like $1 
					OR c.birthday like $1 
					OR c.remarks like $1 
					OR o.nickname like $1
					OR u.account like $1);`
	searchParam = "%" + searchParam + "%"
	rows, stmt, err := util.Query(sql, searchParam)
	if stmt != nil {
		defer stmt.Close()
	}
	if rows != nil {
		defer rows.Close()
	}
	if err != nil {
		logs.Print("From:dealSearchFriend.util.Query:---", err.Error())
		info = "From:dealSearchFriend.util.Query:---error"
		code = errDBQueryFailed
		ok = false
		return info, code, nil, ok
	}
	for rows.Next() {
		var friend friendInfo
		err = rows.Scan(&friend.FriendID, &friend.FriendIcon, &friend.Nickname)
		if err != nil {
			logs.Print("From:dealSearchFriend.rows.Scan:---", err.Error())
			info = "From:dealSearchFriend.rows.Scan:---error"
			code = errRowsScan
			ok = false
			return info, code, nil, ok
		}
		friendsInfo = append(friendsInfo, friend)
	}

	return info, code, friendsInfo, ok
}
