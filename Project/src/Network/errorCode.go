package Network

// Network errorCode for JSON
// 如需使用自定义的errorCode，按照规则添加即可
// 命名规则： err + 自定义名称 = 与现存错误码不冲突的错误码
// 			2xx 代表成功
// 			4xx 代表前端请求有错误
// 			5xx 代表后端处理异常

const (
	Success = 200
	// 250之后的错误码用于/ws接口，具体请看fs_websocket.go内的定义
)

//客户端请求错误
const (
	errBadAPI = iota + 400
	errAccountError
	errAccountNotExist
	errPasswordWrong
	errArgsAllEmpty
	errEmtpyID
	errNegativeInt
	errRequestEntityTooLarge //请求的东西太大(pageSize 太大)
	errEmptyParameter

	errNoPermission //410
	errAssertType
	errInvalidArgLength
	errWrongContentType
	errArgIsNotNumber
	errGroupIDNotExist
	errAccountAndPasswordError
	errInvalidID
	errInvalidAccount
	errInvalidPassword

	errInvalidTime // 420
	errInvalidNickname
	errInvalidIcon
	errInvalidPageIndex
	errInvalidPageSize
	errInvalidCustomInfo
	errInvalidMsgContent
	errInvalidBoolArg
	errInvalidEmptyArgs
	errInvalidTalker

	errNotExistUserInGroup // 430
	errInvalidManager
	errInvalidSender
	errInvalidReceiver
	errInvalidMsg
	errInvalidTokenArg
	errInvalidToken
	errNotString
	errNotInt
	errNotStringArray

	errNotBool // 440
	errNotExistID
	errWrongOldPwd
	errFileNotFound
	errRowsAffected
	errExistSameAccount
	errSearchParam
	errTooManyGroupMember
	errlackParam
	errTooManyManager
	errExistSameParam
)

//服务端错误
const (
	errBadWebsocket = iota + 500
	errMarshalJSONFailed
	errDBQueryFailed
	errDBExecFailed
	errRowsScan
	errOpenDB
	errBeginTx
	errParseForm
	errEndTx
	errParseFileFailed //510
	errTooLargeUploadFile
	errTooLargeChatImage
	errInvalidImageType
	errWriteFileFailed
	errInvalidFileType
)

const (
	mBadAPI              = "没有这个接口，或者接口名称错误！"
	mBadWebsocket        = "创建websockect连接时发生了错误！错误详情："
	mInvalidTokenArg     = "token参数无效，有效的token是长度为32的字符串！"
	mInvalidToken        = "token已失效，请重新获取！"
	mInvalidUserID       = "UserID参数有误，正确的UserID是长度为9位以上数字序列！"
	mNotExistUserID      = "UserID不存在！"
	mInvalidMemberID     = "MemberID参数有误，正确的MemberID是长度为9位以上数字序列！"
	mNotExistMemberID    = "MemberID不存在！"
	mInvalidGroupID      = "GroupID参数有误，正确的GroupID是长度为9位以上数字序列！"
	mNotExistGroupID     = "GroupID不存在！"
	mInvalidObjectID     = "ObjectID参数有误，正确的ObjectID是长度为9位以上数字序列！"
	mNotExistObjectID    = "ObjectID不存在！"
	mInvalidTalkerID     = "TalkerID参数有误，正确的TalkerID是长度为9位以上数字序列！"
	mNotExistTalkerID    = "TalkerID不存在！"
	mNotExistUserInGroup = "当前用户不在此群中！"
	mInvalidAccount      = "Account参数有误，正确的Account是长度为11数字序列！"
	mExistSameAccount    = "Account 已被注册"
	mInvalidPassword     = "Password参数有误，正确的Password是长度为6~20的字符序列，可包含数字、大小写字母及符号,.-_=+!@#$%^&*()[]{} "
	mInvalidTime         = "Time参数有误，正确的Time应采用此格式：yyyy-mm-dd hh:mm:ss，日期与时间之间有一个空格。"
	mInvalidNickname     = "Nickname参数有误，正确的Nickname是长度为1~18的字符序列！"
	mInvalidIcon         = "Icon参数有误，正确的Icon是长度为1~18的字符序列！"
	mInvalidPageIndex    = "PageIndex参数有误，正确的PageIndex是长度为1~6的正整数！"
	mInvalidPageSize     = "PageSize参数有误，正确的PageSize是长度为1~3的正整数！"
	mInvalidCustomInfo   = "CustomInfo参数有误，正确的CustomInfo是长度为0~1024的JSON序列!"
	mInvalidMsgContent   = "MsgContent参数有误，正确的MsgContent是长度为1~512的JSON序列！"
	mInvalidBoolArg      = "Bool类型的参数有误，不是正确的布尔型参数！"
	mInvalidEmptyArgs    = "参数不能为空，请检查相关参数！并检查Content-Type是否为application/x-www-form-urlencoded。"
	mInvalidChat         = "UserID和TalkerID相同，消息发送失败，用户不能发送消息给自己！"
	mInvalidManager      = "此用户不是管理员，不可以删除群组！"
	mInvalidSender       = "亲，消息发送人应该是您本人哦！不要冒充别人，好吗？"
	mInvalidReceiver     = "亲，把消息发给自己是不是很有意思呢？求您别给服务器增加负担啦！"
	mInvalidMsg          = "Message参数为空或消息内容的长度异常，消息长度应为1~512！"
	mParseFileFailed     = "解析文件失败，错误详情："
	mTooLargeUploadFile  = "文件太大！聊天文件的大小请控制在1G以内。"
	mTooLargeChatImage   = "聊天图片的文件大小请控制在10M以内，太大的图片请以文件方式发送！"
	mInvalidImageType    = "您发送的文件不是有效的图片文件，请检查文件类型！"
	mInvalidFileType     = "文件格式错误！"
	mWriteFileFailed     = "文件写入时发生了错误，错误详情："
	mWrongOldPwd         = "旧密码不正确！"
)
