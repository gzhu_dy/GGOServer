package Network

import (
	"SQL/util"
	"encoding/json"
	"logs"
)

func alterObjectData(clientID string, args interface{}) (response *ServerResponse) {

	var code = Success
	var info = "success"
	var information interface{}

	defer func() {
		response = &ServerResponse{
			Method:  "/alterObjectData",
			Code:    code,
			Message: info,
			Result:  information,
		}
	}()

	parameterMap, ok := args.(map[string]interface{})
	if !ok {
		code = errInvalidEmptyArgs
		info = mInvalidEmptyArgs
		return
	}

	objectID, ok := parameterMap["objectID"].(string)
	if !ok {
		code = errInvalidID
		info = "From:alterObjectData:---objectID is null"
		return
	}

	nickname, ok := parameterMap["nickname"].(string)
	if !ok {
		code = errInvalidNickname
		info = "From:alterObjectData:---nickname is null"
		return
	}
	icon, ok := parameterMap["icon"].(string)
	if !ok {
		code = errInvalidIcon
		info = "From:alterObjectData:---icon is null"
		return
	}
	custom, ok := parameterMap["customInfo"].(string)
	if !ok {
		code = errInvalidCustomInfo
		info = "From:alterObjectData:---customInfo is null"
		return
	}

	info, code, ok = checkOutAlterObjectData(objectID, nickname, icon, custom)
	if !ok {
		return
	}

	data := []byte(custom)
	var customMap map[string]interface{}
	err := json.Unmarshal(data, &customMap)
	if err != nil {
		code = errMarshalJSONFailed
		logs.Print("From:alterObjectData.Unmarshal:---", err.Error())
		info = "From:alterObjectData.Unmarshal:---json.Unmarshal error:---" + err.Error()
	}

	customByte, err := json.Marshal(customMap)
	if err != nil {
		code = errMarshalJSONFailed
		logs.Print("From:alterObjectData.Marshal:---", err.Error())
		info = "From:alterObjectData.Marshal:---json.Marshal error"
	}

	info, code, ok = updateObject(objectID, nickname, icon, customByte)

	return
}

func updateObject(id string, nickname string, icon string, custom []byte) (string, int, bool) {

	info := "success"
	ok := true
	code := Success
	if id == "" {
		logs.Print("From:alterObjectData:--- id输入有误,可能路径有误")
		info = "objectid  is null"
		return info, errInvalidID, false
	}

	if icon == "" || icon == "\"\"" {
		icon = "default icon"
	}

	if nickname == "" {
		nickname = " "
	}

	sql := "update objects set nickname = $1, iconurl = $2,custom = $3 where id = $4"
	result, err := util.Exec(sql, nickname, icon, custom, id)
	if err != nil {
		logs.Print("From:alterObjectData.Exec:---", err.Error())
		info = "From:alterObjectData.Exec:--- error"
		code = errDBExecFailed
		ok = false
		return info, code, ok
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		logs.Print("From:alterObjectData.RowsAffected:---", err.Error())
		info = "From:alterObjectData.RowsAffected:---error"
		code = errRowsAffected
		ok = false
		return info, code, ok
	}
	if rowsAffected <= 0 {
		info = "From:alterObjectData.RowsAffected:---update rows is 0"
		code = errDBExecFailed
		ok = false
		return info, code, ok
	}
	return info, code, ok
}

func checkOutAlterObjectData(objectID string, nickName string, icon string, customInfo string) (string, int, bool) {
	info := "success"
	code := Success
	ok := true

	if ok, code, info = checkOutId(objectID); !ok {
		info = "From:alterObjectData.checkOutAlterObjectData:---objectID is invlid"
		return info, code, ok
	}

	if ok, code, info = checkOutNickName(nickName); !ok {
		info = "From:alterObjectData.checkOutAlterObjectData:---nickname is invlid"
		return info, code, ok
	}

	if ok, code, info = checkOutJsonInfo(customInfo); !ok {
		info = "From:alterObjectData.checkOutAlterObjectData:---customInfo is invlid"
		return info, code, ok
	}
	info, code, ok = isObject(objectID)
	if !ok {
		info = "From:alterObjectData.checkOutAlterObjectData:---objectID is invlid because objectID is not exist"
		return info, code, ok
	}

	return info, code, ok
}
