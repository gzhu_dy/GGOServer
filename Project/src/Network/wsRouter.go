package Network

import (
	"fmt"
	"net/http"
	"strings"
)

// const addr = "120.25.208.100:8023"

const addr = ":8001"

/*
websocket建立过程概要：
	1.initWebSocket
	2.addClient
	3.request method,由wsRouter来进行路由处理
	4.deleteClient (掉线)
	5.close (同账号登录)
*/

// websocket接口都在这里部署路由
func wsRouter(clientID string, req ClientRequest) {
	var res *ServerResponse
	switch req.Method {
	//--------------------------------------------------dy
	case "/createGroup", "/addGroupMember", "/addGroupMemberList", "/deleteGroupMember":
		res = dyRouter(req.Method, clientID, req.Args)
		//--------------------------------------------------Ficow Shen
	case "/chatMessageList":
		res = chatMessageList(clientID, req.Args)
	case "/sessionList":
		res = sessionList(clientID, req.Args)
	case "/sendTextMessage":
		res = sendTextMessage(clientID, req.Args)

		//--------------------------------------------------Pismery
	case "/getObjectData":
		res = getObjectData(clientID, req.Args)
	case "/deleteGroup":
		res = deleteGroup(clientID, req.Args)
	case "/alterObjectData":
		res = alterObjectData(clientID, req.Args)
	case "/searchFriend":
		res = searchFriend(clientID, req.Args)
	case "/groupManagerList":
		res = groupManagerList(clientID, req.Args)
		//--------------------------------------------------Decaf
	case "/getFriendList":
		res = getFriendList(clientID, req.Args)
	case "/getGroupList":
		res = getGroupList(clientID, req.Args)
	case "/getGroupMember":
		res = getGroupMember(clientID, req.Args)
	case "/setGroupManager":
		res = setGroupManager(clientID, req.Args)
	case "/searchGroup":
		res = searchGroup(clientID, req.Args)
	default:
		dispatchSystemMsgToClient(clientID, req.Method, errBadAPI, mBadAPI)
		return
		// case "/链接":
		// 	yourFunc(req.Args)
		// }
	}
	writeResponseToClient(clientID, res)
}

// websocket & 文件上传下载服务
type router struct {
}

func (r router) ServeHTTP(w http.ResponseWriter, req *http.Request) {

	var (
		code   int
		msg    string
		result interface{}
	)
	configHTTP(w, req)
	switch req.URL.Path {

	//--------------------------------------------------Ficow Shen
	case "/wsLogin":
		wsLogin(w, req)
		return
	case "/uploadFile":
		code, msg, result = uploadFile(w, req)
	case "/uploadFileTest":
		uploadFileTest(w, req)
		return
		//-------------------------------------------------Pismery
	case "/alterPassword":
		code, msg, result = alterPassword(w, req)
		//--------------------------------------------------Decaf
	case "/register":
		code, msg, result = register(w, req)
	//--------------------------------------------------dy
	case "/sendSystemMessage":
		sendSystemMessage(w, req)
		return
	case "/sendSystemMessagePage":
		test_sendSystemMessage(w, req)
		return
	case "/setData":
		setData(w, req)
		return
	case "/dbPage":
		dbPage(w, req)
		return
	case "/alterIcon":
		code, msg, result = alterIcon(w, req)
	default:
		w.Header().Del("Content-Type")
		if strings.HasPrefix(req.URL.Path, "/eyJTZW5kZXJJRC") {
			getFile(w, req)
			return
		}
		http.ServeFile(w, req, "./public"+req.URL.Path)
		return
	}
	writeHTTPResult(w, code, msg, result)
}

func test_sendSystemMessage(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html;charset=utf8")
	w.Write([]byte(`<html>
<head>
<title>发送系统消息</title>
</head>
<body>
<h1>当前在线用户数:` + fmt.Sprint(onlineNum()) + `</h1>
<form  action="/sendSystemMessage" method="post">
 message: <input type="text" name="message" value=""/><br>
 <input type="submit" value="send" /><br>
</form>
</body>
</html>`))
}
