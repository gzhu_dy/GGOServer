package Network

import (
	"fmt"
	"logs"
	"net/http"
	"time"
)

func sendSystemMessage(w http.ResponseWriter, req *http.Request) {
	before := time.Now()
	errNum := 0
	sendNum := 0
	defer func() {
		fmt.Fprintln(w, "deal", "time", time.Now().Sub(before), "errNum:", errNum, "sendNum:", sendNum)
	}()
	message := req.FormValue("message")
	if message == "" {
		w.Write([]byte("message is empty!"))
		return
	}
	lockk <- true
	defer func() {
		<- lockk
	}()
	for key, val := range wsClients {
		err := val.conn.WriteMessage(1, []byte("system message to "+key+": "+message))
		if err != nil {
			logs.Print(err.Error())
			errNum++
			val.conn.Close()
			delete(wsClients, key)
			continue
		}
		sendNum++
	}
}
