package Network

func testCreateGroup() {
	var test []testData = []testData{
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"aaa",
		"公告":"aaaa",
		"类型":"bbbb",
		"地址":"cccc",
		"网站":"dddd",
		"群介绍":"eeee"
	}
}
`, expCode: Success},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"bbb",
		"公告":"新进群的改备注"
	}
}
`, expCode: Success},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"啊啊啊啊啊啊啊bbbbbbbbbbbbbbbbb",
		"公告":"新进群的改备注"
	}
}
`, expCode: errInvalidNickname},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"公告":"新进群的改备注"
	}
}
`, expCode: errAssertType},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"bbb",
		"公告":"aaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab"
	}
}
`, expCode: errInvalidCustomInfo},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"bbb",
		"aaa":"bbbbbbbbbbbbbbbbb"
	}
}
`, expCode: errInvalidCustomInfo},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"bbb",
		"公告":123
	}
}
`, expCode: errAssertType},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"bbb",
		"公告":"新进群的改备注"
	}
}
`, expCode: Success},
		{
			id: "100000000",
			data: ` 
{
	"Method":"/createGroup",
	"Args":{
		"nickname":"bbb",
		"公告":"新进群的改备注"
	}
}
`, expCode: errNoPermission},
	}
	dyTest(test)
}
