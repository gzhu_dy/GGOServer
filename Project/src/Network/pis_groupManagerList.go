package Network

import (
	"SQL/util"
	"logs"
	"strconv"
)

type groupManagerInfo struct {
	UserID    string `json:"userID"`
	Nickname  string `json:"nickname"`
	IconURL   string `json:"iconURL"`
	IsManager string `json:"IsManager"`
}

func groupManagerList(clientID string, args interface{}) (response *ServerResponse) {

	var code = Success
	var info = "success"
	var information interface{}

	defer func() {
		response = &ServerResponse{
			Method:  "/groupManagerList",
			Code:    code,
			Message: info,
			Result:  information,
		}
	}()

	parameterMap, ok := args.(map[string]interface{})
	if !ok {
		code = errInvalidEmptyArgs
		info = mInvalidEmptyArgs
		return
	}

	groupID, ok := parameterMap["groupID"].(string)
	if !ok {
		code = errInvalidID
		info = "From:groupManagerList:---groupID is null"
		return
	}
	pageSize, ok := parameterMap["pageSize"].(string)
	if !ok {
		code = errInvalidID
		info = "From:groupManagerList:---pageSize is null"
		return
	}
	pageIndex, ok := parameterMap["pageIndex"].(string)
	if !ok {
		code = errInvalidID
		info = "From:groupManagerList:---pageIndex is null"
		return
	}

	if info, code, ok = checkOutGroupManangerList(groupID, pageSize, pageIndex); !ok {
		return
	}

	pageIndexValue, err := strconv.Atoi(pageIndex)
	if err != nil {
		logs.Print("From:checkOutPageIndex:---", err.Error())
		info = "From:groupManagerList.strconv.Atoi:--- pageIndex is invalid"
		return
	}
	pageSizeValue, err := strconv.Atoi(pageSize)
	if err != nil {
		logs.Print("From:checkOutPageIndex:---", err.Error())
		info = "From:groupManagerList.strconv.Atoi:--- pageSize is invalid"
		return
	}

	info, code, information, ok = dealGroupManagerList(groupID, pageSizeValue, pageIndexValue)
	return
}

func checkOutGroupManangerList(groupID, pageSize, pageIndex string) (info string, code int, ok bool) {
	info = "success"
	code = Success
	ok = true

	if ok, code, info = checkOutId(groupID); !ok {
		info = "From:checkOutGroupManangerList.checkOutId:----id is invalid"
		return
	}
	if ok, code, info = checkOutPageIndex(pageIndex); !ok {
		info = "From:checkOutGroupManangerList.checkOutPageIndex:----pageIndex is invalid"
		return
	}
	if ok, code, info = checkOutPageSize(pageSize); !ok {
		info = "From:checkOutGroupManangerList.checkOutPageSize:----pageSize is invalid"
		return
	}

	if info, code, ok = isGroup(groupID); !ok {
		info = "From:checkOutGroupManangerList.isGroup:---groupID is invalid because groupID is not group"
		return
	}
	return
}

func dealGroupManagerList(groupID string, pageSize, pageIndex int) (info string, code int, result interface{}, ok bool) {
	info = "success"
	code = Success
	ok = true
	var groupManagerInfos []groupManagerInfo

	sql := `select m.userid,m.ismanager,o.nickname,o.iconurl
			from members m,objects o 
			where m.groupid = $1 and o.id = m.userid
			order by m.userid
			limit $2 offset $3`
	rows, stmt, err := util.Query(sql, groupID, pageSize, pageSize*pageIndex)
	if stmt != nil {
		defer stmt.Close()
	}
	if rows != nil {
		defer rows.Close()
	}
	if err != nil {
		logs.Print("From:dealGroupManagerList.util.Query:---", err.Error())
		info = "From:dealGroupManagerList.util.Query:---error"
		code = errDBQueryFailed
		ok = false
		return info, code, nil, ok
	}
	for rows.Next() {
		var groupManagerInfo groupManagerInfo
		err = rows.Scan(&groupManagerInfo.UserID, &groupManagerInfo.IsManager, &groupManagerInfo.Nickname, &groupManagerInfo.IconURL)
		if err != nil {
			logs.Print("From:dealGroupManagerList.rows.Scan:---", err.Error())
			info = "From:dealGroupManagerList.rows.Scan:---error"
			code = errRowsScan
			ok = false
			return info, code, nil, ok
		}
		groupManagerInfos = append(groupManagerInfos, groupManagerInfo)
	}

	return info, code, groupManagerInfos, ok
}
