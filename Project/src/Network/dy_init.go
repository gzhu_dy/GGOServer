package Network

import (
	"fmt"
	"net/http"
	"net/url"
)

type responseJSON struct {
	Code    int
	Message string
	Result  interface{}
}

func initRequestJson(js url.Values) map[string]interface{} {
	requestJson := make(map[string]interface{})
	for key, val := range js {
		requestJson[key] = val[0]
	}
	return requestJson
}

func initHandle(w http.ResponseWriter, req *http.Request) error {

	err := req.ParseForm()
	if err != nil {
		return err
	}
	return nil
}

func errorJson(err error) string {
	return `{
				"code":` + fmt.Sprint(errMarshalJSONFailed) + `,
				"Message":"` + err.Error() + `",
				"Result":null
			}`
}
