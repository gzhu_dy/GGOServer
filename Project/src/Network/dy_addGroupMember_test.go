package Network

func testAddGroupMember() {
	var test []testData = []testData{
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006",
		"memberIDs":[
		"100000004",
		"100000007",
		"100000008"
		]
	}
}
`, expCode: Success},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"1006",
		"memberIDs":[
		"100000004"
		]
	}
}
`, expCode: errInvalidID},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000000",
		"memberIDs":[
		"100000004"
		]
	}
}
`, expCode: errInvalidID},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"10000000",
		"memberIDs":[
		"100000004"
		]
	}
}
`, expCode: errNotExistID},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":100000006,
		"memberIDs":[
		"100000004"
		]
	}
}
`, expCode: errAssertType},
		{
			id: "100000001",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006",
		"memberIDs":[
		"100000004"
		]
	}
}
`, expCode: errNoPermission},
		{
			id: "1000000010",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006",
		"memberIDs":[
		"100000004"
		]
	}
}
`, expCode: errNoPermission},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006"
	}
}
`, expCode: errAssertType},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006",
		"memberIDs":[
			"100000006",
			"10000001"
		]
	}
}
`, expCode: errInvalidID},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006",
		"memberIDs":[
			"100000007",
			"100000004",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errInvalidID},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006",
		"memberIDs":[
			"100000009",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errInvalidID},
		{
			id: "100000000",
			data: `
{
	"Method":"/addGroupMember",
	"Args":{
		"groupID":"100000006",
		"memberIDs":[
			"100000a",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errInvalidID},
		//num memberIDs > 200
	}
	dyTest(test)
}
