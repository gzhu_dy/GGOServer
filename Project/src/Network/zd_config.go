// zd_config
package Network

import (
	"SQL/db"
	"encoding/json"
	"regexp"
	"strconv"
)

func checkArgs(key string, val string) (Msg string, Code int) {
	msg := ""
	code := Success
	if key != "customInfo" {

		if len(val) == 0 {
			msg = mInvalidEmptyArgs
			code = errInvalidEmptyArgs
			return msg, code
		}
	}
	_, err := strconv.Atoi(val)
	switch {
	case key == "userID":
		if err != nil || len(val) != 9 {
			msg = mInvalidUserID
			code = errInvalidID
		}
	case key == "pageIndex":
		if err != nil || len(val) > 6 {
			msg = mInvalidPageIndex
			code = errInvalidPageIndex
		}
	case key == "pageSize":
		if err != nil || len(val) > 3 {
			msg = mInvalidPageSize
			code = errInvalidPageSize
		}
	case key == "groupID":
		if err != nil || len(val) != 9 {
			msg = mInvalidGroupID
			code = errInvalidID
		}
	case key == "isManager":
		if val != "true" || val != "false" {
			msg = mInvalidBoolArg
			code = errInvalidBoolArg
		}
	case key == "password":
		reg := regexp.MustCompile("^[\\da-zA-Z-_\\.\\?\\!\\,\\@\\#\\$\\%\\^\\&\\*\\(\\)\\=\\+\\[\\]\\{\\}]{6,20}$")
		ok := reg.MatchString(val)
		if !ok {
			msg = mInvalidPassword
			code = errInvalidPassword
		}
	case key == "customInfo":
		reg := regexp.MustCompile(("^{.*\\r*\\n*}"))
		ok := reg.MatchString(val)
		if !ok {
			msg = mInvalidCustomInfo
			code = errInvalidCustomInfo
		}
		var tem interface{}
		customlist := []byte(val)
		err = json.Unmarshal(customlist, &tem)
		if err != nil {
			msg = mInvalidCustomInfo
			code = errInvalidCustomInfo
		}
	case key == "account":
		if err != nil || len(val) != 11 {
			msg = mInvalidAccount
			code = errInvalidAccount
		}
	default:
		code = errAssertType
		msg = "参数名错误 :" + key
	}
	return msg, code
}

func checkisExistGroup(groupID string) (msg string, code int) {
	rows, err := db.RowQuery("select *from groups where id=$1", groupID)
	if err != nil {
		code = errDBQueryFailed
		msg = err.Error()
		return
	}
	if !rows.Next() {
		code = errGroupIDNotExist
		msg = mNotExistGroupID
		return
	}
	return "", Success
}

func checkOwnerofGroup(ownerID string, groupID string) (msg string, code int) {
	sql := "select *from groups where id =$1 and owner =$2"
	rows, err := db.RowQuery(sql, groupID, ownerID)
	if err != nil {
		code = errDBQueryFailed
		msg = err.Error()
		return
	}
	if !rows.Next() {
		code = errNoPermission
		msg = ownerID + " is not owner of group " + groupID
		return
	}
	return "", Success
}
func checkisExistUser(userID interface{}) (msg string, code int) {
	sql := "select *from users where id =$1"
	rows, err := db.RowQuery(sql, userID)
	if err != nil {
		code = errDBQueryFailed
		msg = err.Error()
		return
	}
	if !rows.Next() {
		code = errNotExistID
		msg = mNotExistUserID
		return
	}
	return "", Success
}
func checkUserInGroup(userID interface{}, groupID string) (msg string, code int) {
	sql := "select *from members where groupid =$1 and userid=$2"
	rows, err := db.RowQuery(sql, groupID, userID)
	if err != nil {
		code = errDBQueryFailed
		msg = err.Error()
		return
	}
	if !rows.Next() {
		code = errNotExistUserInGroup
		msg = userID.(string) + "不在此群中"
		return
	}
	return "", Success
}
