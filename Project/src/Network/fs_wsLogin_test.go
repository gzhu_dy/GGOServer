package Network

import (
	"encoding/json"
	"io/ioutil"

	"logs"

	"net/http"

	"github.com/gorilla/websocket"
)

func testWsLogin() {
	test := []testData{
		{
			data: map[string]string{
				"account":  "15602309617",
				"password": "123456",
			},
			expCode: Success,
		},
		{
			data: map[string]string{
				"account":  "",
				"password": "123467",
			},
			expCode: errInvalidEmptyArgs,
		},
		{
			data: map[string]string{
				"account":  "15602309617",
				"password": "",
			},
			expCode: errInvalidEmptyArgs,
		},
		{
			data: map[string]string{
				"account":  "100000000",
				"password": "123467",
			},
			expCode: errInvalidID,
		},
		{
			data: map[string]string{
				"account":  "100000000aa",
				"password": "123467",
			},
			expCode: errInvalidID,
		},
		{
			data: map[string]string{
				"account":  "15602309617",
				"password": "123",
			},
			expCode: errInvalidPassword,
		},
		{
			data: map[string]string{
				"account":  "15602309617",
				"password": "123467",
			},
			expCode: errPasswordWrong,
		},
	}
	for _, val := range test {
		valData, ok := val.data.(map[string]string)
		if !ok {
			panic("not map[string]string")
		}
		wsUrl := "ws://" + socket + "/wsLogin?account=" + valData["account"] + "&password=" + valData["password"]
		//logs.Print(wsUrl)
		conn, resp, err := websocket.DefaultDialer.Dial(wsUrl, nil)
		if err != nil {
			panic(err)
		}
		if resp.StatusCode != http.StatusSwitchingProtocols {
			logs.Print(resp)
			body, err := ioutil.ReadAll(resp.Body)
			data := make(map[string]interface{})
			if err != nil {
				panic(err)
			}
			err = json.Unmarshal(body, data)
			if err != nil {
				panic(err)
			}
			continue
		}
		defer resp.Body.Close()
		var res ServerResponse
		err = conn.ReadJSON(&res)
		if err != nil {
			panic(err)
		}
		val.code = res.Code
		val.msg = res.Message
		val.result = res.Result
		if val.code != val.expCode {
			logs.Print(val)
			panic(val)
		}
	}

	conn, _, err := websocket.DefaultDialer.Dial("ws://"+socket+"/wsLogin?account=15602309617&password=123456", nil)
	if err != nil {
		panic(err)
	}
	var res ServerResponse
	err = conn.ReadJSON(&res)
	if err != nil {
		panic(err)
	}
	logs.Print(res)
	token, ok := res.Result.(map[string]interface{})
	if !ok {
		panic("token type assert failed")
	}
	websocketToken, ok = token["Token"].(string)
	if !ok {
		panic("token['Token'] type assert failed")
	}
	err = conn.ReadJSON(&res)
	if err != nil {
		panic(err)
	}
	logs.Print(res)
	if res.Code != noticeLoginAgain {
		panic(err)
	}
	go func() {
		for {
			err := conn.ReadJSON(&res)
			if err != nil {
				return
			}
		}
	}()
}
