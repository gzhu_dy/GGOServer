package Network

func testDeleteGroupMember() {
	var test []testData = []testData{
		{
			id: "100000000",
			data: `
{
	"Method": "/deleteGroupMember",
	"Args": {
		"groupID": "100000006",
		"memberIDs": [
			"100000004",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: Success},
		{
			id: "100000000",
			data: `
{
	"Method": "/deleteGroupMember",
	"Args": {
		"groupID": "1000000",
		"memberIDs": [
			"100000004",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errNotExistID},
		{
			id: "100000001",
			data: `
{
	"Method": "/deleteGroupMember",
	"Args": {
		"groupID": "100000006",
		"memberIDs": [
			"100000004",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errNoPermission},
		{
			id: "100000000",
			data: `
{
	"Method": "/deleteGroupMember",
	"Args": {
		"groupID": "100000006",
		"memberIDs": [
			"100000004",
			"100000004",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errInvalidID},
		{
			id: "100000000",
			data: `
{
	"Method": "/deleteGroupMember",
	"Args": {
		"groupID": "100000006",
		"memberIDs": [
			"100000004",
			"100000000",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errInvalidID},
		{
			id: "100000000",
			data: `
{
	"Method": "/deleteGroupMember",
	"Args": {
		"groupID": "100000006",
		"memberIDs": [
			"100000004",
			"100000007",
			"100000008"
		]
	}
}
`, expCode: errDBExecFailed},
	}
	dyTest(test)
}
