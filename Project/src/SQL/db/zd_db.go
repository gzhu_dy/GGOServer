package db

import (
	"database/sql"
	"logs"

	_ "github.com/lib/pq"
)

//var db *sql.DB = nil
var err error

const (
	dbport   = "5432"
	dbuser   = "postgres"
	password = "123456"
	dbname   = "postgres"
	sslmode  = "disable"
)

/*func Opendb() error {
	var err error = nil
	if db == nil {
		sqlopen := "port=" + dbport + " user=" + dbuser + " password=" + password + " dbname=" + dbname + " sslmode=" + sslmode
		db, err = sql.Open("postgres", sqlopen)
		if err != nil {
			logs.Print("sql.Open failed :", err.Error())
		}
	}
	return err
}

func Getdb() (*sql.DB, error) {
	var err error = nil
	if db == nil {
		err = Opendb()
		if err != nil {
			logs.Print("Getdb failed :", err.Error())
		}
	}
	return db, err
}

func Closedb(db *sql.DB) error {
	err := db.Close()
	if err != nil {
		logs.Print("closrdb failed :", err.Error())
	} else {
		logs.Print("closedb success")
	}
	return err
}*/

var dbQueue chan *sql.DB

func Init(queue chan *sql.DB) {
	dbQueue = queue
}
func Open() *sql.DB {
	return <-dbQueue
}

func Close(db *sql.DB) {
	dbQueue <- db
}

func RowQuery(Sql string, args ...interface{}) (*sql.Rows, error) {
	db := Open()
	defer Close(db)
	stmt, err := db.Prepare(Sql)
	if err != nil {
		logs.Print("rowQuery db.Prepare error:", err.Error())
		return nil, err
	}
	defer stmt.Close()
	rows, err := stmt.Query(args...)
	if err != nil {
		logs.Print("rowQuery db.Query error:", err.Error())
		return nil, err
	}
	return rows, err
}

func RowExec(sql string, args ...interface{}) error {
	db := Open()
	defer Close(db)
	stmt, err := db.Prepare(sql)
	if err != nil {
		logs.Print("rowExec db.Prepare error:", err.Error())
		return err
	}
	defer stmt.Close()
	_, err = stmt.Exec(args...)
	if err != nil {
		logs.Print("rowExec db.Exec error:", err.Error())
		return err
	}
	return nil
}
func TxQuery(tx *sql.Tx, Sql string, args ...interface{}) (*sql.Stmt, *sql.Rows, error) {
	stmt, err := tx.Prepare(Sql)
	if err != nil {
		logs.Print("txQuery tx.Prepare error:", err.Error())
		return nil, nil, err
	}
	rows, err := stmt.Query(args...)
	if err != nil {
		logs.Print("txQuery stmt.Query error:", err.Error())
		return nil, nil, err
	}
	return stmt, rows, err
}

func TxExec(tx *sql.Tx, Sql string, args ...interface{}) error {
	stmt, err := tx.Prepare(Sql)
	defer stmt.Close()
	if err != nil {
		logs.Print("txExec tx.Prepare error:", err.Error())
		return err
	}
	_, err = stmt.Exec(args...)
	if err != nil {
		logs.Print("txExec tx.Exec error:", err.Error())
		return err
	}
	return err
}
