sudo sysctl -w net.ipv4.ip_local_port_range=2000\ 65535
sudo sysctl -w net.ipv4.tcp_syncookies=0
sudo sysctl -w net.ipv4.tcp_max_syn_backlog=65535
sudo sysctl -w net.ipv4.tcp_max_tw_buckets=65535
sudo sysctl -w net.ipv4.tcp_fin_timeout=30
sudo sysctl -w net.ipv4.tcp_tw_reuse=1
sudo sysctl -w net.ipv4.tcp_timestamps=1
sudo sysctl -w net.ipv4.tcp_tw_recycle=1
ulimit -n 130000
rm log*
setsid ./emb >> log 2>&1 &
