
"use strict";
/*--------------------------begin group-data---------------------------*/
function getGroupMember(id) {
	ws.send(JSON.stringify({
		Method: "/getGroupMember",
		Args: {
			groupID: "100000005",
			pageIndex: groupMemberIndex+"",
			pageSize: groupMemberSize+"",
		}
	}));
}
function groupData(ev,id) {
	ev.stopPropagation();
	bodyContainer.children().hide();
	setCookie("page","group-data");
	$(".group-data").show();
	$(".group-data .body-body").html(`
	<div class="dropdown-menu">
		<a class="button-green">管理成员</a>
		<a class="button-green">修改群属性</a>
		<a class="button-green">设置管理员</a>
		<a id="logout" class="button-red">删除群组</a>
	</div>
	<div class="body-modal" onclick="hideModal('.group-data');"> </div>
	<div class="divider"></div>
	<div class="data-body">
	</div>
	`);
	getObjectData(id);
	getGroupMember(id);
}
function showGroupMember(members) {
	var dataBody=$(".group-data .data-body");
	var	html="";
	html+=`<a class="keyword">`+members.length+`</a>
	<a class="word">个群成员</a>
	<div style="width:100%;height:17px;"></div>
	`
	for(var i=0;i<members.length;i++) {
		var data=members[i];
		var memberID=data["MemberID"]
		,	memberIcon=data["MemberIcon"]
		,	isManager=data["IsManager"]
		;
		html+=`<div class="icon" style="background-image:url('`+memberIcon+`');" onclick="personData(event,'`+memberID+`');">`
		if(isManager) html+=`<img class="manager" src="/image/manager.png"></img>`
		html+=`</div>`
	}
	html+=`<div class="clear"></div>`
	dataBody.prepend(html);
}
function showGroupData(data) {
	var owner=data["owner"]
	,	custom=data["custom"]
	,	icon=data["icon"]
	,	nickname=data["nickname"]
	,	objectID=data["objectID"]
	;
	var html="";
	html+=`<div class="btn chat" onclick="showChatDialog('`+nickname+`','`+objectID+`',false);"><a class="send"></a></div>`
	html+=`<img class="icon" src="`+icon+`" onclick="showBigImage(this);"></img>
	<a class="nickname">`+nickname+`</a>
	<a class="id">`+objectID+`</a>`
	$(".group-data .body-body").append(html);
	html=`<div style="width:100%;height:19px;"></div>
	<a class="keyword">群</a>
	<a class="word">属性</a>
	<div style="width:100%;height:26px;"></div>
	`
	//custom=new Object();
	//custom["地点"]="广州大学城广州大学";
	//custom["职责"]="Golang后端开发";
	var customJSON=JSON.parse(custom);
	for(var key in customJSON) {
		html+=`<a class="key">`+key+`：</a>`;
		html+=`<a class="value">`+customJSON[key]+`</a>`;
		html+=`<br>`;
	}
	$(".group-data .data-body").append(html);
}
/*---------------------------end group-data-----------------------------*/