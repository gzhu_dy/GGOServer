"use strict";
/*--------------------------begin person-data--------------------------*/
$("#icon").click(function(event) {
	personData(event,userID);
});
function getObjectData(id) {
	ws.send(JSON.stringify({
		Method: "/getObjectData",
		Args: {
			objectID: id
		}
	}));
}
function showObjectData(data) {
	if(data["owner"]) showGroupData(data);
	else showUserData(data);
}
function showUserData(data) {
	var account=data["account"]
	,	custom=data["custom"]
	,	icon=data["icon"]
	,	nickname=data["nickname"]
	,	objectID=data["objectID"]
	,	html=""
	;
	var title=$(".person-data .title");
	if(userID==objectID) {
		title.html("个人资料");
	}else {
		title.html(nickname+"的资料");
		html+=`<div class="btn chat" onclick="showChatDialog('`+nickname+`','`+objectID+`',true);"><a class="send"></a></div>`
	}
	//custom=new Object();
	//custom["性别"]="男";
	//custom["兴趣爱好"]="编程";
	html+=`<img class="icon" src="`+icon+`" onclick="showBigImage(this);"></img>
	<a class="nickname">`+nickname+`</a>
	<a class="id">`+objectID+`</a>`;
	var customJSON=JSON.parse(custom);
	for(var key in customJSON) {
		html+=`<a class="key">`+key+`：</a>`;
		html+=`<a class="value">`+customJSON[key]+`</a>`;
	}
	$(".person-data .body-body").append(html);
}
function personData(ev,id) {
	ev.stopPropagation();
	bodyContainer.children().hide();
	setCookie("page","person-data");
	$(".person-data").show();
	var html="";
	if(id==userID) {
		html+=`<a class="btn change" onclick="alertPersonData();">修改</a>`;
	}
	html+=`<div class="divider"></div>`;
	$(".person-data .body-body").html(html);
	getObjectData(id);
}
/*--------------------------end person-data-----------------------------*/