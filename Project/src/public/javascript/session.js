"use strict";
/*---------------------begin session-----------------------*/
$(document).ready(function() {
	$(".dropdown").mouseenter(function() {
		var target=$(this).children(".tips");
		target.show();
		setTimeout(function() {
			target.hide();
		},1000);
	});
	footerContact.click(function(){
		$("#list-chat").hide();
		$("#list-contact").show();
		footerChat.removeAttr("active");
		footerContact.attr("active","");
		getFriendsList();
	});
	footerChat.click(function() {
		$("#list-contact").hide();
		$("#list-chat").show();
		footerChat.attr("active","");
		footerContact.removeAttr("active");
		sessionList();
	});
	btnFriends.click(function() {
		btnGroups.removeAttr("active");
		btnFriends.attr("active","");
		contactGroups.hide();
		contactFriends.show();
		getFriendsList();
	});
	btnGroups.click(function() {
		btnFriends.removeAttr("active");
		btnGroups.attr("active","");
		contactFriends.hide();
		contactGroups.show();
		getGroupsList();
	});
	contactFriends.scroll(function() {
		var viewH =$(this).height(),//可见高度
        contentH =$(this).get(0).scrollHeight,//内容高度
        scrollTop =$(this).scrollTop();//滚动高度
        //if(contentH - viewH - scrollTop <= 0) { //到达底部100px时,加载新内容
        if(scrollTop/(contentH - viewH)>=0.95){ //到达底部100px时,加载新内容
			// 这里加载数据..
			getFriendsList();
        }
	});
	contactGroups.scroll(function() {
		var viewH =$(this).height(),//可见高度
        contentH =$(this).get(0).scrollHeight,//内容高度
        scrollTop =$(this).scrollTop();//滚动高度
        //if(contentH - viewH - scrollTop <= 0) { //到达底部100px时,加载新内容
        if(scrollTop/(contentH - viewH)>=0.95){ //到达底部95%时,加载新内容
			// 这里加载数据..
			getGroupsList();
        }
	});
});
function sessionList() {
	ws.send(JSON.stringify({
		"Method": "/sessionList",
		"Args": {}
	}));
}
function friendSessionsList(sessions){
	//console.log(sessions);
	for(var i=0;i<sessions.length;i++) {
		var json=sessions[i];
		var msg=json["Msg"]
		,	talkerIconURL=json["TalkerIconURL"]
		,	talkerName=json["TalkerName"]
		,	talkerID=json["TalkerID"]
		,	senderID=json["SenderID"]
		,	msgID=json["MsgID"]
		,	msgType=json["MsgType"]
		,	createTime=json["CreateTime"]
		,	unreadNum=json["UnreadNum"]
		;
		if(msgType=="image") {
			msg="[图片]";
		}
		var time=new Date(createTime);
		var html=` <div class="list-row" onclick="showChatDialog('`+talkerName+`','`+talkerID+`',true);" > 
			<img class="icon" src="`+talkerIconURL+`" onclick="personData(event,'`+talkerID+`');"/> `;
		html+=`<a class="nickname">`+talkerName+`</a>`;
		if(unreadNum && unreadNum!=0) html+=`<span class="bubble">`+unreadNum+`</span>`;
		html+=`<a class="time">`+(time.getMonth()+1)+"-"+time.getDate()+" "+time.getHours()+":"+time.getMinutes()+`</a>`;
		html+=`<a class="msg">`+ (msg.length>26?msg.substring(0,24)+'..':msg)+`</a> </div>`;
		$("#chat-content").append(html);
	}
}
function groupSessionsList(sessions) {
	for(var i=0;i<sessions.length;i++) {
		var json=sessions[i];
		var msg=json["Msg"]
		,	groupIconURL=json["GroupIconURL"]
		,	groupName=json["GroupName"]
		,	groupID=json["GroupID"]
		,	senderID=json["SenderID"]
		,	msgID=json["MsgID"]
		,	msgType=json["MsgType"]
		,	createTime=json["CreateTime"]
		,	unreadNum=json["UnreadNum"]
		,	senderName=json["SenderName"]
		;
		if(msgType=="image") {
			msg="[图片]";
		}
		msg=senderName+"："+msg;
		var time=new Date(createTime);
		var html=` <div class="list-row" onclick="showChatDialog('`+groupName+`','`+groupID+`',false);" > 
			<img class="icon" src="`+groupIconURL+`" onclick="groupData(event,'`+groupID+`')";></img> `;
		html+=`<a class="nickname">`+groupName+`</a>`;
		if(unreadNum && unreadNum!=0) html+=`<span class="bubble">`+unreadNum+`</span>`;
		html+=`<a class="time">`+(time.getMonth()+1)+"-"+time.getDate()+" "+time.getHours()+":"+time.getMinutes()+`</a>`;
		html+=`<a class="msg">`+ (msg.length>26?msg.substring(0,24)+'..':msg)+`</a> </div>`;
		$("#chat-content").append(html);
	}
}
// /chatMessageList接口 发送到服务端
function chatMessageList(id,isUser) {
	ws.send(JSON.stringify({
		Method: "/chatMessageList",
		Args: {
			pageIndex: messageIndex,
			pageSize: messageSize,
			talkerID: id,
			talkerIsUser:	isUser 
		}
	}));
}
/*---------------------end session--------------------------*/
/*-----------------------begin contact--------------------------------*/
function friendContactList(list) {
	if(!list){
		alertWarning("已显示了全部好友！");
		return;
	}
	for(var i=0;i<list.length;i++) {
		var listRow=list[i];
		var friendID=listRow["FriendID"]
		,	nickname=listRow["Nickname"]
		,	friendIcon=listRow["FriendIcon"]
		;
		var html=`<div class="list-row" onclick="showChatDialog('`+nickname+`','`+friendID+`',true);">
			<img class="icon" src="`+friendIcon+`" onclick="personData(event,'`+friendID+`');"></img>
			<a class="nickname">`+nickname+`</a>
			</div>`;
		contactFriends.append(html);
	}
	friendIndex++;
}
function groupContactList(list) {
	if(!list) {
		alertWarning("已显示全部群组！");
		return;
	}
	for(var i=0;i<list.length;i++) {
		var listRow=list[i];
		var groupID=listRow["GroupID"]
		,	nickname=listRow["Nickname"]
		,	groupIcon=listRow["GroupIcon"]
		;
		var html=`<div class="list-row" onclick="showChatDialog('`+nickname+`','`+groupID+`',false);">
			<img class="icon" src="`+groupIcon+`" onclick="groupData(event,'`+groupID+`');"></img>
			<a class="nickname">`+nickname+`</a>
			</div>`;
		contactGroups.append(html);
	}
	groupIndex++;
}
function getFriendsList() {
	ws.send(JSON.stringify({
		Method: "/getFriendList",
		Args: {
			pageIndex: friendIndex+"",
			pageSize: friendSize+""
		}
	}));
}
function getGroupsList() {
	ws.send(JSON.stringify({
		Method: "/getGroupList",
		Args: {
			pageIndex: groupIndex+"",
			pageSize: groupSize+"" 
		}
	}));
}
/*--------------------------end contact---------------------------------*/