"use strict";
/*---------------------begin register-----------------------*/
$(document).ready(function() {
	$("#registerPage").click(function(){
		bodyContainer.children().hide();
		setCookie("page","register");
		register.show();
	});
	$("#registerBack").click(function(){
		register.hide();
		login.show();
	});
	$("#registerSubmit").click(function(){
		var registerAccount=$("#registerAccount")
		,	registerPassword=$("#registerPassword");
		var account=registerAccount.val()
		,	password=registerPassword.val();
		if(account=="") {
			registerAccount.focus();
			alertWarning("帐号不能为空！");
			return;
		}
		if(password=="") {
			registerPassword.focus();
			alertWarning("密码不能为空！");
			return;
		}
		var accountPartten=new RegExp("[0-9]{11}");
		if(!accountPartten.test(account)) {
			registerAccount.focus();
			alertWarning("帐号应为十一位数字(手机号)！");
			return ;
		}
		$.post("/register",
			{
				account:account,
				password:password
			},
			function(data,status) {//返回的data已经是json，不用再转换
				if(status != "success") {
					alertDanger(status+"\n"+data);
					return;
				}
				if(data["Code"]!=200) {
					alertDanger(data["Message"]);
					return;
				}
				alertSuccess("注册成功！");
				register.hide();
				login.show();
				inputAccount.val(account);
				inputPassword.val(password);
			}
		);
	});
	$("#color").mousedown(function(){
		$("#colorValue").text($("#color").val());
	});
});
/*----------------------end register--------------------------*/