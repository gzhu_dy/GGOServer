"use strict";
/*---------------------begin login---------------------------*/
$(document).ready(function() {
	function loginMessage(idx) {
		switch(idx) {
			case 1: return "帐号不能为空！";
			case 2: return "帐号应为十一位数字(手机号码)！";
			case 3: return "密码不能为空！";
			default:
			return "error";
		}
	}
	function checkLogin() {
		accountImage.attr("disabled","");
		loginSubmit.attr("disabled","");
		var account=inputAccount.val()
		,	password=inputPassword.val();
		if(!account) {
			inputAccount.focus();
			return 1;
		}
		var accountPartten=/[0-9]{11}/g;
		if(!accountPartten.test(account)) {
			inputAccount.focus();
			return 2;
		}
		accountImage.removeAttr("disabled");
		passwordImage.attr("disabled","");
		if(!password) {
			inputPassword.focus();
			return 3;
		}
		passwordImage.removeAttr("disabled");
		loginSubmit.removeAttr("disabled");
		return 0;
	}
	$("#loginSubmit").click(function() {
		document.cookie="";
		if(ws) {
			alertDanger("已登录！");
			return;
		}
		var idx=checkLogin();
		if(idx==1 || idx==2 || idx==3) {
			alertWarning(loginMessage(idx));
			if(idx==1 || idx==2) {
				inputAccount.focus();
			} else if(idx==3) {
				inputPassword.focus();
			}
			return;
		}
		var wsUrl="ws";
		if(location.protocol=="https:") {
			wsUrl+="s";
		}
		wsUrl+="://"+location.host+"/wsLogin?account="+inputAccount.val()+"&password="+inputPassword.val();
		//var wsUrl="ws://"+"emb.mobi"+"/wsLogin?account="+inputAccount.val()+"&password="+inputPassword.val();
		if (!("WebSocket" in window)) {
			// 浏览器不支持 WebSocket
			alert("登录失败！您的浏览器不支持 WebSocket!");
			return ;
		}
		
		// 打开一个 web socket
		ws = new WebSocket(wsUrl);
		ws.onopen = function() {
			login.hide();
			session.show();//刚登录会自动推送会话列表
		};
		ws.onmessage = dealMessage;
		ws.onclose = function() { 
			alertWarning("已退出登录！");
			//alert("已退出登录！");
			location.reload();
		};
		ws.onerror = function(evt) {
			alertWarning(evt.data);
		}
		ws.onerror = function() {
			alertDanger("error");
		}
	});
	$("#logout").click(function(){
		if(!ws) {
			alert("请先登录！");
			return;
		}
		ws.close();
		location.reload();
	});
	inputAccount.keydown(function() {
		var res=checkLogin();
		if(res==1 || res==2) {
			accountImage.attr("disabled","");
			loginSubmit.attr("disabled","");
			return;
		}
		if(res==0) {
			loginSubmit.removeAttr("disabled");
		}
	});
	inputPassword.keydown(function() {
		var res=checkLogin();
		if(res==3) {
			passwordImage.attr("disabled","");
			loginSubmit.attr("disabled","");
			return;
		}
		if(res==0) {
			loginSubmit.removeAttr("disabled");
		}
	});
	inputAccount.blur(function() {
		var res=checkLogin();
		if(res==1 || res==2) {
			alertDanger(loginMessage(res));
			accountImage.attr("disabled","");
			loginSubmit.attr("disabled","");
			return;
		}
		if(res==0) {
			loginSubmit.removeAttr("disabled");
		}
	});
	inputPassword.blur(function() {
		var res=checkLogin();
		if(res==3) {
			alertDanger(loginMessage(res));
			passwordImage.attr("disabled","");
			loginSubmit.attr("disabled","");
			return;
		}
		if(res==0) {
			loginSubmit.removeAttr("disabled");
		}
	});
	setTimeout(function() {
		checkLogin();
		inputAccount.focus();
	},1000);
});
function dealMessage(evt) 
{ 
	var received_msg = evt.data;
	/*$("#message").append("<li>"+received_msg+"</li>");*/
	var serverResponse=JSON.parse(received_msg);
	if(!serverResponse) {
		alertWarning("json 解析失败！");
		return ;
	}
	var res=serverResponse["Result"];
	if(serverResponse["Method"]=="/wsLogin") {
		var msg=serverResponse["Message"];
		switch(serverResponse["Code"])
		{
			case 252:
			alertSuccess(msg);
			break;
			case 253:
			alertDanger(msg);
			break;
			case 254:
			alertWarning(msg);
			break;
			default:
			alertWarning(msg);
			break;
		}
		iconURL=res["IconURL"];
		nickname=res["UserName"];
		userID=res["UserID"];
		$("#icon").attr("src",iconURL);
		$("#nickname").text(nickname);
		$("#list-chat").show();
		footerContact.removeAttr("disabled");
		return ;
	}
	if(serverResponse["Code"] != 200) {
		alertWarning("error:"+serverResponse["Message"]);
		return ;
	}
	console.log(serverResponse);
	console.log(res);
	switch (serverResponse["Method"]) {
		case "/token":
			token=res["Token"];
			break;
		case "/sessionList":
		{
			var friendSessions=res["FriendSessions"]
			,	groupSessions=res["GroupSessions"];
			$("#chat-content").html("");
			friendSessionsList(friendSessions);
			groupSessionsList(groupSessions);
			break;
		}
		case "/getFriendList":
			friendContactList(res);
			break;
		case "/getGroupList":
			groupContactList(res);
			break;
		case "/chatMessageList":
			showChatMessageList(res)
			break;
		case "/sendTextMessage":
			alertSuccess(serverResponse["Message"]);
			break;
		case "/receiveMessage":
			receiveMessage(res);
			sessionList();
			break;
		case "/getObjectData":
			console.log(document.cookie);
			var page=getCookie("page");
			console.log(document.cookie);
			console.log(page);
			switch (page) {
				case "person-data":
					showUserData(res[0]);
					break;
				case "group-data":
					showGroupData(res[0]);
					break;
				case "alert-person-data":
					showAlertPersonData(res[0]);
					break;
				case "alert-group-data":
					showAlertGroupData(res[0]);
					break;
				default:
					alertDanger("deal /getObjectData on page "+page);
					break;
			}
			break;
		case "/getGroupMember":
			showGroupMember(res);
			break;
		default:
			alertWarning(serverResponse["Method"]+"接口正在开发中！");
			break;
	}
};

/*---------------------end login---------------------------*/