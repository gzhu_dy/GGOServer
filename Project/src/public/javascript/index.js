"use strict";
var ws=null 
,	messageBoxNum=0
,	token
,	nickname
,	iconURL
,	userID
,	friendIndex=0
,	friendSize=20
,	groupIndex=0
,	groupSize=20
,	groupMemberIndex=0
,	groupMemberSize=20
,	currentTalkerID
,	currentTalkerIsUser
,	messageSize=20
,	messageIndex=0
;
var inputAccount=$("#inputAccount")
,	inputPassword=$("#inputPassword")
,	login=$("#login")
,	session=$("#session")
,	register=$("#register")
,	loginSubmit=$("#loginSubmit")
,	footerContact=$("#footer-contact")
,	footerChat=$("#footer-chat")
,	listChat=$("#list-chat")
,	listContact=$("#list-Contact")
,	contactFriends=$("#contact-friends")
,	contactGroups=$("#contact-groups")
,	btnFriends=$("#btn-friends")
,	btnGroups=$("#btn-groups")
,	accountImage=$(".login .image.account")
,	passwordImage=$(".login .image.password")
,	bodyContainer=$(".body-container")
,	chatDialog=$("#chat-dialog")
;
var width=window.innerWidth
|| document.documentElement.clientWidth
|| document.body.clientWidth;

var height=window.innerHeight
|| document.documentElement.clientHeight
|| document.body.clientHeight;
function alertClose(id){
	function close(){
		$("#"+id).remove();
	}
	setTimeout(close,10000);
}
function alertSuccess(str){
	var id="messageBox"+messageBoxNum;
	messageBoxNum++;
	$("#messageBox").append(`
	<div id="`+id+`" class="alert-success">
		<a onclick="$('#`+id+`').remove()">&times;</a>
		`+str+`
	</div>
	`);
	alertClose(id);
}
function alertWarning(str){
	var id="messageBox"+messageBoxNum;
	messageBoxNum++;
	$("#messageBox").append(`
	<div id="`+id+`" class="alert-warning">
		<a onclick="$('#`+id+`').remove()">&times;</a>
		`+str+`
	</div>
	`);
	alertClose(id);
}
function alertDanger(str){
	var id="messageBox"+messageBoxNum;
	messageBoxNum++;
	$("#messageBox").append(`
	<div id="`+id+`" class="alert-danger">
		<a onclick="$('#`+id+`').remove()">&times;</a>
		`+str+`
	</div>
	`);
	alertClose(id);
}
function setCenter(tag){
	tag.css({"margin-left":parseFloat(tag.parent().width()/2-tag.width()/2)});
	tag.css({"margin-top":parseFloat(tag.parent().height()/2-tag.height()/2)});
}
function back() {
	bodyContainer.children().hide();
	setCookie("page","session");
	session.show();
	sessionList();
}
function hideModal(page) {
	$(page+" .dropdown-menu").hide();
	$(page+" .body-modal").hide();
}
function showMenu(page) {
	$(page+" .body-modal").toggle();
	$(page+" .dropdown-menu").toggle();
}
function setCookie(cname,cvalue)
{
	document.cookie=cname+"="+cvalue;
}
 
function getCookie(cname)
{
	var cookie=document.cookie;
	var beg=cookie.indexOf(cname+"=");
	if(beg<0) return "";
	var end=cookie.substr(beg).indexOf(';');
	if(end<0) end=cookie.length;
	else end=beg+end;
	return cookie.substring(beg+cname.length+1,end);
}
$(document).ready(function(){
	/*alertDanger(navigator.platform);
	alertSuccess(navigator.appName);
	alertWarning(navigator.appVersion);
	alertDanger(navigator.language);
	alertDanger(window.innerHeight);
	alertDanger(window.innerWidth);*/
	//$("body").append($("html").html());
	if(screen.availWidth<width) width=screen.availWidth;
	if(screen.availHeight<height) height=screen.availHeight;
	//$("html").css({"width":width,"height":height});
	//$("body").css({"width":width,"height":height});
	//alertSuccess(screen.availWidth+" "+screen.availHeight+" "+screen.width+" "+screen.height);
	alertSuccess(window.width+" "+window.height+" "+window.availWidth+" "+window.availHeight+" "+window.innerWidth+" "+window.innerHeight);
	//alertSuccess(width+" "+height);
	//alert($(".body-container").height()+" "+$(".body-container").width());
	//setCenter($(".body-container"));

	/*setInterval(function() {
		$("#colorValue").css("background",$("#color").val());
	},1000);*/
});