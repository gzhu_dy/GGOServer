
负责人：沈学良

登录服务器，建立websocket连接：
URL:  /wsLogin（GET/POST）


注意：
1.通过websocket登录后，服务端就会立即自动返回用户的会话列表，无需手动访问/sessionList获取会话列表；
2.若登录时获取会话列表失败，请手动调用/sessionList接口获取会话列表；
3.有关/sessionList的内容，请查看/sessionList接口文档；



request:
account string,
password string





例如当前登录的用户的帐号为13800138000,
请求/wsLogin示例：

访问ws://emb.mobi/wsLogin?account=13800138000&password=123456


1.服务端返回Token：（如果登录存在疑问，请查看接口规范文档）
{
  "Method": "/token",
  "Code": 200,
  "Message": "",
  "Result": {
    "Token": "8705e2c70bea2e3b63a0478b585545c2"
  }
}


2.登录成功，服务端返回数据示例：
{
  "Method": "/wsLogin",
  "Code": 252,
  "Message": "LOGIN SUCCESS",
  "Result": {
    "UserID": "100000002",
    "IconURL": "/img/icon/defaultIcon",
    "UserName": "用户1"
  }
}



Code释义：
251 好友不在线
252 用户登录成功
253 用户被人挤下线
254 用户在挤别人下线