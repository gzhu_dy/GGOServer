负责人：钟志丹
现更改为：

获取群列表
URL:  /getGroupList（websocket）


例如当前登录的用户的ID为100000003,

请求示例：
{
  "Method": "/getGroupList",
  "Args": {
    "pageIndex": "0",
    "pageSize": "20"
  }
}

返回：
{
   "Method":"/getGroupList",
   "Code":200,"Message":"获取群列表成功",
   "Result":[
		{"GroupID":"100000005","Nickname":"嵌入式讨论群","GroupIcon":"/img/icon/defaultIcon"},
		{"GroupID":"100000006","Nickname":"吹水群6","GroupIcon":"/img/icon/defaultIcon"}
     ]
}



之前版本为：
URL:  /getGroupList
群列表:
s userID,pageIndex int,pageSize int
r Message string,
 	[GroupID string,GroupIcon string,Nickname string]


请求(GET/POST)
userID ="10001"
pageIndex="0"
pageSize="20"


返回：
{
  
   "Code": 200,

   "Message": "获取群列表成功",

   "Result": [
    
             {

                "GroupID": "10004",
                
                "Nickname": "emb",

                "GroupIcon": "icon2"
              
                  },

             {

                "GroupID": "10005",
                
                "Nickname": "discuss",
                
               "GroupIcon": "icon5"
               
              }

     ]

}