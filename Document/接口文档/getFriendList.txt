负责人：钟志丹
现更改为：

获取好友列表
URL:  /getFriendList（websocket）


例如当前登录的用户的ID为100000000,

请求示例/getFriendList:
{
  "Method": "/getFriendList",
  "Args": {
    "pageIndex": "0",
    "pageSize": "20"
  }
}

返回：
{ 
	"Method": "/getFriendList",
	"Code": 200, 
	"Message": "获取好友列表成功",
	"Result": [
	{"FriendID": "100000001", "Nickname": "dy", "FriendIcon": "/icon/default.ico" },
	{"FriendID": "100000002", "Nickname": "Ficow", "FriendIcon": "/icon/default.ico" },
	{"FriendID": "100000003", "Nickname": "Decaf", "FriendIcon": "/icon/default.ico" },
	{"FriendID": "100000004", "Nickname": "Pismery", "FriendIcon": "/icon/default.ico" },
	{"FriendID": "100000007", "Nickname": "kemin", "FriendIcon": "/icon/default.ico" },
	{"FriendID": "100000008", "Nickname": "jimmy", "FriendIcon": "/icon/default.ico" }
	]
}




之前版本为：
URL:  /getFriendList
好友列表：
s userID int，pageIndex int,pageSize int
r Message string,
	[FriendID string,FriendIcon string,Nickname string]


请求（GET/POST）
userID = "10001"
pageIndex="0"
pageSize ="20"


返回
{
  
   "Code": 200,
 
   "Message": "获取好友列表成功",
 
   "Result": [

              {

                "FriendID": "10001",

                "Nickname": "dy",

                "FriendIcon": "icon"

              },

             {  
                
              "FriendID": "10002",  
                 
               "Nickname": "xl",
                
               "FriendIcon": "icon1"
               },
              {

                "FriendID": "10003",
                
                "Nickname": "user3",
                
                "FriendIcon": "icon3"

               }
        ]

}
